package cartao

import entityCartao "gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/enterprise_business/entities/cartao"

type ICartaoInteractor interface {
	Criar(cartaoCliente entityCartao.ICartaoCliente) CartaoResponse
}
