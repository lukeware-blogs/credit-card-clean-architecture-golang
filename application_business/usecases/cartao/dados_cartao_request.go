package cartao

import "gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/application_business/usecases/gerador_cartao"

type DadosCartaoRequest struct {
	Tipo      string
	Documento string
	Email     string
	Telefone  string
	Cartao    gerador_cartao.CartaoResponse
}
