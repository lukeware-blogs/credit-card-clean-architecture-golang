package cartao

import (
	"errors"
	"github.com/stretchr/testify/assert"
	usecaseScore "gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/application_business/usecases/score"
	entityCartao "gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/enterprise_business/entities/cartao"
	entityCliente "gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/enterprise_business/entities/cliente"
	cartao2 "gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/frameworks_drivers/repository/cartao"
	"gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/frameworks_drivers/utility"
	"testing"
)

func Test_devera_criar_um_cartao_valido(t *testing.T) {
	t.Parallel()

	// --------------------------- preparando teste -----------------------------------
	assertions := assert.New(t)
	iCartaoPresenter := new(MockCartaoPresenter)
	iAntiFraundInteractor := new(MockAntiFraundGateway)
	iScoreInteractor := new(MockScoreInteractor)
	iGeradorCartaoInteractor := new(MockGeradorCartaoInteractor)

	validator := utility.NewValidator()
	cartao := entityCartao.NewCartao("Diego Silva Morais", 4, "Gold", 4.99, cartao2.MASTER_CARD, validator)
	cliente := entityCliente.NewCliente(entityCliente.PF, "999.999.999-99", "diegomorais@gmail.com", "+55 (19) 9 9999-9999", validator)
	cartaoCliente := entityCartao.NewCartaoCliente(cartao, cliente, validator)

	cartaoInteractor := NewCartaoInteractor(iCartaoPresenter, iAntiFraundInteractor, iScoreInteractor, iGeradorCartaoInteractor)
	// --------------------------- preparando teste -----------------------------------

	// --------------------------- preparando comportamento do teste -----------------------------------
	iCartaoPresenter.On("Sucesso").Return(CartaoResponse{
		Success: "parabéns Diego Silva Morais! seu novo cartão foi gerado, confira sua caixa de entreda",
	})
	iAntiFraundInteractor.On("ClienteFraudador").Return(entityCartao.CLIENTE_NAO_FRAUDADOR, nil)
	iScoreInteractor.On("ObterScore").Return(usecaseScore.ScoreResponse{
		Nivel: 1,
	}, nil)
	iGeradorCartaoInteractor.On("Gerar").Return(nil)
	// --------------------------- preparando comportamento do teste -----------------------------------

	// --------------------------- Executando teste -----------------------------------
	cartaoResponse := cartaoInteractor.Criar(cartaoCliente)
	// --------------------------- Executando teste -----------------------------------

	// --------------------------- Validando teste -----------------------------------
	assertions.Equal("parabéns Diego Silva Morais! seu novo cartão foi gerado, confira sua caixa de entreda", cartaoResponse.Success)
	// --------------------------- Validando teste -----------------------------------
}

func Test_devera_validar_cartao_cpf_invalido(t *testing.T) {
	t.Parallel()

	assertions := assert.New(t)
	iCartaoPresenter := new(MockCartaoPresenter)
	iAntiFraundInteractor := new(MockAntiFraundGateway)
	iScoreInteractor := new(MockScoreInteractor)
	iGeradorCartaoInteractor := new(MockGeradorCartaoInteractor)

	validator := utility.NewValidator()
	cartao := entityCartao.NewCartao("Diego Silva Morais", 4, "Gold", 4.99, cartao2.MASTER_CARD, validator)
	cliente := entityCliente.NewCliente(entityCliente.PF, "", "diegomorais@gmail.com", "+55 (19) 9 9999-9999", validator)
	cartaoCliente := entityCartao.NewCartaoCliente(cartao, cliente, validator)

	cartaoInteractor := NewCartaoInteractor(iCartaoPresenter, iAntiFraundInteractor, iScoreInteractor, iGeradorCartaoInteractor)

	iCartaoPresenter.On("Falha").Return(CartaoResponse{
		Error: "documento não informado",
	})

	cartaoResponse := cartaoInteractor.Criar(cartaoCliente)

	assertions.Equal("documento não informado", cartaoResponse.Error)

}

func Test_devera_valida_cartao_com_erro_servico_anti_fraude(t *testing.T) {
	t.Parallel()

	assertions := assert.New(t)
	iCartaoPresenter := new(MockCartaoPresenter)
	iAntiFraundInteractor := new(MockAntiFraundGateway)
	iScoreInteractor := new(MockScoreInteractor)
	iGeradorCartaoInteractor := new(MockGeradorCartaoInteractor)

	validator := utility.NewValidator()
	cartao := entityCartao.NewCartao("Diego Silva Morais", 4, "Gold", 4.99, cartao2.MASTER_CARD, validator)
	cliente := entityCliente.NewCliente(entityCliente.PF, "999.999.999-99", "diegomorais@gmail.com", "+55 (19) 9 9999-9999", validator)
	cartaoCliente := entityCartao.NewCartaoCliente(cartao, cliente, validator)

	cartaoInteractor := NewCartaoInteractor(iCartaoPresenter, iAntiFraundInteractor, iScoreInteractor, iGeradorCartaoInteractor)

	iAntiFraundInteractor.On("ClienteFraudador").Return(entityCartao.CLIENTE_FRAUDADOR, errors.New("ops, houve uma falha no sistema. tente mais tarde"))
	iCartaoPresenter.On("Falha").Return(CartaoResponse{
		Error: "ops, houve uma falha no sistema. tente mais tarde",
	})

	cartaoResponse := cartaoInteractor.Criar(cartaoCliente)

	assertions.Equal("ops, houve uma falha no sistema. tente mais tarde", cartaoResponse.Error)

}

func Test_devera_valida_cartao_com_cliente_fraudador(t *testing.T) {
	t.Parallel()

	assertions := assert.New(t)
	iCartaoPresenter := new(MockCartaoPresenter)
	iAntiFraundInteractor := new(MockAntiFraundGateway)
	iScoreInteractor := new(MockScoreInteractor)
	iGeradorCartaoInteractor := new(MockGeradorCartaoInteractor)

	validator := utility.NewValidator()
	cartao := entityCartao.NewCartao("Diego Silva Morais", 4, "Gold", 4.99, cartao2.MASTER_CARD, validator)
	cliente := entityCliente.NewCliente(entityCliente.PF, "999.999.999-99", "diegomorais@gmail.com", "+55 (19) 9 9999-9999", validator)
	cartaoCliente := entityCartao.NewCartaoCliente(cartao, cliente, validator)

	cartaoInteractor := NewCartaoInteractor(iCartaoPresenter, iAntiFraundInteractor, iScoreInteractor, iGeradorCartaoInteractor)

	iAntiFraundInteractor.On("ClienteFraudador").Return(entityCartao.CLIENTE_FRAUDADOR, nil)
	iCartaoPresenter.On("Falha").Return(CartaoResponse{
		Error: "desculpe, não foi possivel criar o seu cartão, tente novamente em 6 meses",
	})

	cartaoResponse := cartaoInteractor.Criar(cartaoCliente)

	assertions.Equal("desculpe, não foi possivel criar o seu cartão, tente novamente em 6 meses", cartaoResponse.Error)
}

func Test_devera_valida_cartao_com_falha_na_validacao_score(t *testing.T) {
	t.Parallel()

	assertions := assert.New(t)
	iCartaoPresenter := new(MockCartaoPresenter)
	iAntiFraundInteractor := new(MockAntiFraundGateway)
	iScoreInteractor := new(MockScoreInteractor)
	iGeradorCartaoInteractor := new(MockGeradorCartaoInteractor)

	validator := utility.NewValidator()
	cartao := entityCartao.NewCartao("Diego Silva Morais", 4, "Gold", 4.99, cartao2.MASTER_CARD, validator)
	cliente := entityCliente.NewCliente(entityCliente.PF, "999.999.999-99", "diegomorais@gmail.com", "+55 (19) 9 9999-9999", validator)
	cartaoCliente := entityCartao.NewCartaoCliente(cartao, cliente, validator)

	cartaoInteractor := NewCartaoInteractor(iCartaoPresenter, iAntiFraundInteractor, iScoreInteractor, iGeradorCartaoInteractor)

	iAntiFraundInteractor.On("ClienteFraudador").Return(entityCartao.CLIENTE_NAO_FRAUDADOR, nil)
	iScoreInteractor.On("ObterScore").Return(usecaseScore.ScoreResponse{}, errors.New("ops, houve uma falha no sistema. tente mais tarde"))
	iCartaoPresenter.On("Falha").Return(CartaoResponse{
		Error: "ops, houve uma falha no sistema. tente mais tarde",
	})

	cartaoResponse := cartaoInteractor.Criar(cartaoCliente)

	assertions.Equal("ops, houve uma falha no sistema. tente mais tarde", cartaoResponse.Error)
}

func Test_devera_valida_cartao_com_cliente_score_alto(t *testing.T) {
	t.Parallel()

	assertions := assert.New(t)
	iCartaoPresenter := new(MockCartaoPresenter)
	iAntiFraundInteractor := new(MockAntiFraundGateway)
	iScoreInteractor := new(MockScoreInteractor)
	iGeradorCartaoInteractor := new(MockGeradorCartaoInteractor)

	validator := utility.NewValidator()
	cartao := entityCartao.NewCartao("Diego Silva Morais", 4, "Gold", 4.99, cartao2.MASTER_CARD, validator)
	cliente := entityCliente.NewCliente(entityCliente.PF, "999.999.999-99", "diegomorais@gmail.com", "+55 (19) 9 9999-9999", validator)
	cartaoCliente := entityCartao.NewCartaoCliente(cartao, cliente, validator)

	cartaoInteractor := NewCartaoInteractor(iCartaoPresenter, iAntiFraundInteractor, iScoreInteractor, iGeradorCartaoInteractor)

	iCartaoPresenter.On("Falha").Return(CartaoResponse{
		Error: "desculpe, não foi possivel criar o seu cartão, tente novamente em 6 meses",
	})
	iAntiFraundInteractor.On("ClienteFraudador").Return(entityCartao.CLIENTE_NAO_FRAUDADOR, nil)
	iScoreInteractor.On("ObterScore").Return(usecaseScore.ScoreResponse{
		Nivel: 5,
	}, nil)

	cartaoResponse := cartaoInteractor.Criar(cartaoCliente)

	assertions.Equal("desculpe, não foi possivel criar o seu cartão, tente novamente em 6 meses", cartaoResponse.Error)
}

func Test_devera_valida_cartao_com_erro_gerar_cartao(t *testing.T) {
	t.Parallel()

	assertions := assert.New(t)
	iCartaoPresenter := new(MockCartaoPresenter)
	iAntiFraundInteractor := new(MockAntiFraundGateway)
	iScoreInteractor := new(MockScoreInteractor)
	iGeradorCartaoInteractor := new(MockGeradorCartaoInteractor)

	validator := utility.NewValidator()
	cartao := entityCartao.NewCartao("Diego Silva Morais", 4, "Gold", 4.99, cartao2.MASTER_CARD, validator)
	cliente := entityCliente.NewCliente(entityCliente.PF, "999.999.999-99", "diegomorais@gmail.com", "+55 (19) 9 9999-9999", validator)
	cartaoCliente := entityCartao.NewCartaoCliente(cartao, cliente, validator)

	cartaoInteractor := NewCartaoInteractor(iCartaoPresenter, iAntiFraundInteractor, iScoreInteractor, iGeradorCartaoInteractor)

	iCartaoPresenter.On("Falha").Return(CartaoResponse{
		Error: "ops, houve uma falha no sistema. tente mais tarde",
	})
	iAntiFraundInteractor.On("ClienteFraudador").Return(entityCartao.CLIENTE_NAO_FRAUDADOR, nil)
	iScoreInteractor.On("ObterScore").Return(usecaseScore.ScoreResponse{
		Nivel: 1,
	}, nil)
	iGeradorCartaoInteractor.On("Gerar").Return(errors.New("ops, houve uma falha no sistema. tente mais tarde"))

	cartaoResponse := cartaoInteractor.Criar(cartaoCliente)

	assertions.Equal("ops, houve uma falha no sistema. tente mais tarde", cartaoResponse.Error)
}
