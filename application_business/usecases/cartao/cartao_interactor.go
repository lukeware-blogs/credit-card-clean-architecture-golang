package cartao

import (
	"errors"
	usecaseCliente "gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/application_business/usecases/cliente"
	usecaseGeradorCartao "gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/application_business/usecases/gerador_cartao"
	usecaseScore "gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/application_business/usecases/score"
	entityCartao "gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/enterprise_business/entities/cartao"
	utility "gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/frameworks_drivers/utility"
	"gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/interface_adapters/gateway/antifraud"
)

const (
	DESCULPE = "desculpe, não foi possivel criar o seu cartão, tente novamente em 6 meses"
)

type cartaoInteractor struct {
	cartaoPresenter         ICartaoPresenter
	antiFraundGateway       antifraud.IAntiFraudGateway
	scoreInteractor         usecaseScore.IScoreInteractor
	geradorCartaoInteractor usecaseGeradorCartao.IGeradorCartaoInteractor
	validator               utility.IValidator
}

func NewCartaoInteractor(cartaoPresenter ICartaoPresenter,
	antiFraundGateway antifraud.IAntiFraudGateway,
	scoreInteractor usecaseScore.IScoreInteractor,
	geradorCartaoInteractor usecaseGeradorCartao.IGeradorCartaoInteractor) ICartaoInteractor {
	return cartaoInteractor{
		validator:               utility.NewValidator(),
		cartaoPresenter:         cartaoPresenter,
		antiFraundGateway:       antiFraundGateway,
		scoreInteractor:         scoreInteractor,
		geradorCartaoInteractor: geradorCartaoInteractor,
	}
}

func (c cartaoInteractor) Criar(cartaoCliente entityCartao.ICartaoCliente) CartaoResponse {

	err := cartaoCliente.EValido()
	if err != nil {
		return falha(err, c.cartaoPresenter)
	}

	clienteFraudador, antiFraudError := clienteFraudador(cartaoCliente, c.antiFraundGateway)
	if antiFraudError != nil && clienteFraudador != entityCartao.CLIENTE_NAO_REGISTRADO {
		return falha(antiFraudError, c.cartaoPresenter)
	}
	if clienteFraudador == entityCartao.CLIENTE_FRAUDADOR {
		return falha(errors.New(DESCULPE), c.cartaoPresenter)
	}

	scoreResponse, scoreError := buscarScoreCliente(cartaoCliente, c.scoreInteractor)
	if scoreError != nil {
		return c.cartaoPresenter.Falha(scoreError)
	}
	if scoreResponse.Nivel > 3 {
		return falha(errors.New(DESCULPE), c.cartaoPresenter)
	}

	error := gerarCartao(cartaoCliente, scoreResponse.Limite, c.geradorCartaoInteractor)
	if error != nil {
		return falha(scoreError, c.cartaoPresenter)
	}

	return sucesso(cartaoCliente.Cartao().Titular(), c.cartaoPresenter)
}

func sucesso(nomeCliente string, cartaoPresenter ICartaoPresenter) CartaoResponse {
	return cartaoPresenter.Sucesso(nomeCliente)
}

func falha(error error, cartaoPresenter ICartaoPresenter) CartaoResponse {
	return cartaoPresenter.Falha(error)
}

func gerarCartao(cartaoCliente entityCartao.ICartaoCliente, limite float64, geradorCartao usecaseGeradorCartao.IGeradorCartaoGateway) error {
	cartaoClienteRequest := usecaseGeradorCartao.CartaoClienteRequest{
		Tipo:      cartaoCliente.Cliente().Tipo(),
		Documento: cartaoCliente.Cliente().Documento(),
		Email:     cartaoCliente.Cliente().Email(),
		Telefone:  cartaoCliente.Cliente().Telefone(),
		Cartao: usecaseGeradorCartao.CartaoResponse{
			Titular:       cartaoCliente.Cartao().Titular(),
			DiaVencimanto: cartaoCliente.Cartao().DiaVencimento(),
			Limite:        limite,
			NomeCartao:    cartaoCliente.Cartao().NomeCartao(),
			Bandeira:      cartaoCliente.Cartao().Bandeira(),
			Anuidade:      cartaoCliente.Cartao().Anuidade(),
		},
	}
	return geradorCartao.Gerar(cartaoClienteRequest)
}

func buscarScoreCliente(cartaoCliente entityCartao.ICartaoCliente, scoreInteractor usecaseScore.IScoreInteractor) (usecaseScore.ScoreResponse, error) {
	dadosClienteScoreRequest := usecaseCliente.ClienteScoreRequest{
		Tipo:       cartaoCliente.Cliente().Tipo(),
		Documento:  cartaoCliente.Cliente().Documento(),
		Email:      cartaoCliente.Cliente().Email(),
		Telefone:   cartaoCliente.Cliente().Telefone(),
		Estrageiro: false,
	}
	return scoreInteractor.ObterScore(dadosClienteScoreRequest)
}

func clienteFraudador(cartaoCliente entityCartao.ICartaoCliente, antiFraundGateway antifraud.IAntiFraudGateway) (string, error) {
	return antiFraundGateway.ClienteFraudador(cartaoCliente.Cliente().Documento())
}
