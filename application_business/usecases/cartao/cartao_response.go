package cartao

type CartaoResponse struct {
	Success string `json:"success,omitempty"`
	Error   string `json:"error,omitempty"`
}
