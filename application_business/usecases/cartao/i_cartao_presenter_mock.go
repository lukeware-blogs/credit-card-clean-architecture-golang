package cartao

import (
	"github.com/stretchr/testify/mock"
)

type MockCartaoPresenter struct {
	mock.Mock
}

func (mock *MockCartaoPresenter) Falha(err error) CartaoResponse {
	args := mock.Called()
	result := args.Get(0)
	return result.(CartaoResponse)
}
func (mock *MockCartaoPresenter) Sucesso(nomeCliente string) CartaoResponse {
	args := mock.Called()
	result := args.Get(0)
	return result.(CartaoResponse)
}
