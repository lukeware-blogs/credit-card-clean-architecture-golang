package cartao

import (
	"github.com/stretchr/testify/mock"
	useCaseCliente "gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/application_business/usecases/cliente"
	"gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/application_business/usecases/score"
)

type MockScoreInteractor struct {
	mock.Mock
}

func (mock *MockScoreInteractor) ObterScore(request useCaseCliente.ClienteScoreRequest) (score.ScoreResponse, error) {
	args := mock.Called()
	result := args.Get(0)
	return result.(score.ScoreResponse), args.Error(1)
}
