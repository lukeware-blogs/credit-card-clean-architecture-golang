package cartao

import (
	"github.com/stretchr/testify/mock"
	"gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/application_business/usecases/gerador_cartao"
)

type MockGeradorCartaoInteractor struct {
	mock.Mock
}

func (mock *MockGeradorCartaoInteractor) Gerar(request gerador_cartao.CartaoClienteRequest) error {
	args := mock.Called()
	return args.Error(0)
}
