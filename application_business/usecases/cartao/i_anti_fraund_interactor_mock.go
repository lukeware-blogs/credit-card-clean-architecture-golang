package cartao

import (
	"github.com/stretchr/testify/mock"
)

type MockAntiFraundGateway struct {
	mock.Mock
}

func (mock *MockAntiFraundGateway) ClienteFraudador(documento string) (string, error) {
	args := mock.Called()
	result := args.Get(0)
	return result.(string), args.Error(1)
}
