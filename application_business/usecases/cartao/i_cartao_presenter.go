package cartao

type ICartaoPresenter interface {
	Falha(err error) CartaoResponse
	Sucesso(nomeCliente string) CartaoResponse
}
