package cartao_cliente

import (
	"gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/frameworks_drivers/repository/cartao"
	graphModel "gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/graph/model"
)

type cartaoClienteInteractor struct {
	cartaoClientePresenter  ICartaoClientePresenter
	cartaoClienteRepository cartao.ICartaoClienteRepository
}

func NewCartaoClienteInteractor(
	cartaoClientePresenter ICartaoClientePresenter,
	cartaoClienteRepository cartao.ICartaoClienteRepository) ICartaoClienteInteractor {
	return cartaoClienteInteractor{
		cartaoClienteRepository: cartaoClienteRepository,
		cartaoClientePresenter:  cartaoClientePresenter,
	}
}

func (c cartaoClienteInteractor) Todos() ([]*graphModel.CustomerCreditCard, error) {
	cartoes, err := c.cartaoClienteRepository.Todos()
	if err != nil {
		return []*graphModel.CustomerCreditCard{}, err
	}
	return c.cartaoClientePresenter.Success(cartoes), nil
}

func (c cartaoClienteInteractor) PorDocumento(documento string) ([]*graphModel.CustomerCreditCard, error) {
	cartoes, err := c.cartaoClienteRepository.PorDocumento(documento)
	if err != nil {
		return []*graphModel.CustomerCreditCard{}, err
	}
	return c.cartaoClientePresenter.Success(cartoes), nil
}
