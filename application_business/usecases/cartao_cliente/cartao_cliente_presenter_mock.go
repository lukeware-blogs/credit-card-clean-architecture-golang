package cartao_cliente

import (
	"github.com/stretchr/testify/mock"
	"gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/application_business/usecases/gerador_cartao"
	graphModel "gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/graph/model"
)

type MockCartaoClientePresenter struct {
	mock.Mock
}

func (mock *MockCartaoClientePresenter) Success(todos []gerador_cartao.CartaoClienteResponse) []*graphModel.CustomerCreditCard {
	args := mock.Called()
	result := args.Get(0)
	return result.([]*graphModel.CustomerCreditCard)
}
