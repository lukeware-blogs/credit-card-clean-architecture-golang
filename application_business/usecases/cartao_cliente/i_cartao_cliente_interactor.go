package cartao_cliente

import (
	graphModel "gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/graph/model"
)

type ICartaoClienteInteractor interface {
	Todos() ([]*graphModel.CustomerCreditCard, error)
	PorDocumento(documento string) ([]*graphModel.CustomerCreditCard, error)
}
