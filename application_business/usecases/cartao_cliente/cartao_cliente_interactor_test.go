package cartao_cliente

import (
	"errors"
	"github.com/stretchr/testify/assert"
	usecaseGeradorCartao "gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/application_business/usecases/gerador_cartao"
	"gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/enterprise_business/entities/cliente"
	"gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/frameworks_drivers/repository/cartao"
	"gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/graph/model"
	"testing"
)

func Test_devera_buscar_todos_os_cartoes_criados(t *testing.T) {
	t.Parallel()

	assertions := assert.New(t)

	cartaoClienteRepository := new(MockCartaoClienteRepository)
	cartaoClientePresenter := new(MockCartaoClientePresenter)

	cartaoClienteRepository.On("Todos").Return([]usecaseGeradorCartao.CartaoClienteResponse{
		{
			Email:     "diego.morais@labsit.io",
			Documento: "999.999.999-99",
			Telefone:  "+55 19 9 9999-9999",
			Tipo:      cliente.PF,
			Cartao: usecaseGeradorCartao.CartaoResponse{
				Anuidade:      4.99,
				Bandeira:      cartao.MASTER_CARD,
				NomeCartao:    "Platinium Gold",
				Cvc:           "999",
				Serie:         "1234 5678 5432 8765",
				DiaVencimanto: 1,
				Limite:        10000.00,
				Titular:       "Diego Morais",
			},
		},
	}, nil)

	cartaoClientePresenter.On("Success").Return([]*model.CustomerCreditCard{
		{
			Email:    "diego.morais@labsit.io",
			Document: "999.999.999-99",
			Phone:    "+55 19 9 9999-9999",
			Type:     cliente.PF,
			CredCard: &model.CardCredit{
				Annuity:       4.99,
				CardFlag:      cartao.MASTER_CARD,
				CardName:      "Platinium Gold",
				Cvc:           "999",
				Series:        "1234 5678 5432 8765",
				ExpirationDay: 1,
				Limit:         10000.00,
				Cardholder:    "Diego Morais",
			},
		},
	})

	iCartaoClienteInteractor := NewCartaoClienteInteractor(cartaoClientePresenter, cartaoClienteRepository)

	todos, err := iCartaoClienteInteractor.Todos()

	assertions.NoError(err)
	assertions.NotNil(todos)
	assertions.True(len(todos) > 0)

	assertions.Equal("diego.morais@labsit.io", todos[0].Email)
	assertions.Equal("+55 19 9 9999-9999", todos[0].Phone)
	assertions.Equal("999.999.999-99", todos[0].Document)
	assertions.Equal(cliente.PF, todos[0].Type)
	assertions.Equal("Platinium Gold", todos[0].CredCard.CardName)
	assertions.Equal(cartao.MASTER_CARD, todos[0].CredCard.CardFlag)
	assertions.Equal(10000.00, todos[0].CredCard.Limit)
	assertions.Equal(1, todos[0].CredCard.ExpirationDay)
	assertions.Equal("999", todos[0].CredCard.Cvc)
	assertions.Equal("Diego Morais", todos[0].CredCard.Cardholder)

}

func Test_devera_buscar_todos_os_cartoes_criados_por_documento(t *testing.T) {
	t.Parallel()

	assertions := assert.New(t)

	cartaoClienteRepository := new(MockCartaoClienteRepository)
	cartaoClientePresenter := new(MockCartaoClientePresenter)

	cartaoClienteRepository.On("PorDocumento").Return([]usecaseGeradorCartao.CartaoClienteResponse{
		{
			Email:     "diego.morais@labsit.io",
			Documento: "999.999.999-99",
			Telefone:  "+55 19 9 9999-9999",
			Tipo:      cliente.PF,
			Cartao: usecaseGeradorCartao.CartaoResponse{
				Anuidade:      4.99,
				Bandeira:      cartao.MASTER_CARD,
				NomeCartao:    "Platinium Gold",
				Cvc:           "999",
				Serie:         "1234 5678 5432 8765",
				DiaVencimanto: 1,
				Limite:        10000.00,
				Titular:       "Diego Morais",
			},
		},
	}, nil)

	cartaoClientePresenter.On("Success").Return([]*model.CustomerCreditCard{
		{
			Email:    "diego.morais@labsit.io",
			Document: "999.999.999-99",
			Phone:    "+55 19 9 9999-9999",
			Type:     cliente.PF,
			CredCard: &model.CardCredit{
				Annuity:       4.99,
				CardFlag:      cartao.MASTER_CARD,
				CardName:      "Platinium Gold",
				Cvc:           "999",
				Series:        "1234 5678 5432 8765",
				ExpirationDay: 1,
				Limit:         10000.00,
				Cardholder:    "Diego Morais",
			},
		},
	})

	iCartaoClienteInteractor := NewCartaoClienteInteractor(cartaoClientePresenter, cartaoClienteRepository)

	todos, err := iCartaoClienteInteractor.PorDocumento("999.999.999-99")

	assertions.NoError(err)
	assertions.NotNil(todos)
	assertions.True(len(todos) > 0)

	assertions.Equal("diego.morais@labsit.io", todos[0].Email)
	assertions.Equal("+55 19 9 9999-9999", todos[0].Phone)
	assertions.Equal("999.999.999-99", todos[0].Document)
	assertions.Equal(cliente.PF, todos[0].Type)
	assertions.Equal("Platinium Gold", todos[0].CredCard.CardName)
	assertions.Equal(cartao.MASTER_CARD, todos[0].CredCard.CardFlag)
	assertions.Equal(10000.00, todos[0].CredCard.Limit)
	assertions.Equal(1, todos[0].CredCard.ExpirationDay)
	assertions.Equal("999", todos[0].CredCard.Cvc)
	assertions.Equal("Diego Morais", todos[0].CredCard.Cardholder)

}

func Test_devera_simular_error_ao_buscar_todos_os_cartoes_criados(t *testing.T) {
	t.Parallel()

	assertions := assert.New(t)

	cartaoClienteRepository := new(MockCartaoClienteRepository)
	cartaoClientePresenter := new(MockCartaoClientePresenter)

	cartaoClienteRepository.On("Todos").Return([]usecaseGeradorCartao.CartaoClienteResponse{}, errors.New("ops erro ao tentar buscar cartaoes"))

	iCartaoClienteInteractor := NewCartaoClienteInteractor(cartaoClientePresenter, cartaoClienteRepository)

	todos, err := iCartaoClienteInteractor.Todos()

	assertions.EqualError(err, "ops erro ao tentar buscar cartaoes")
	assertions.NotNil(todos)
	assertions.True(len(todos) == 0)

}

func Test_devera_simulcar_com_error_ao_buscar_todos_os_cartoes_criados_por_documento(t *testing.T) {
	t.Parallel()

	assertions := assert.New(t)

	cartaoClienteRepository := new(MockCartaoClienteRepository)
	cartaoClientePresenter := new(MockCartaoClientePresenter)

	cartaoClienteRepository.On("PorDocumento").Return([]usecaseGeradorCartao.CartaoClienteResponse{}, errors.New("ops erro ao tentar buscar cartaoes"))

	iCartaoClienteInteractor := NewCartaoClienteInteractor(cartaoClientePresenter, cartaoClienteRepository)

	todos, err := iCartaoClienteInteractor.PorDocumento("999.999.999-99")

	assertions.EqualError(err, "ops erro ao tentar buscar cartaoes")
	assertions.NotNil(todos)
	assertions.True(len(todos) == 0)
}
