package cartao_cliente

import (
	"gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/application_business/usecases/gerador_cartao"
	graphModel "gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/graph/model"
)

type ICartaoClientePresenter interface {
	Success(todos []gerador_cartao.CartaoClienteResponse) []*graphModel.CustomerCreditCard
}
