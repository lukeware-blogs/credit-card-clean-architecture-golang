package catalogo_cartoes

import (
	"github.com/stretchr/testify/mock"
	"gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/frameworks_drivers/repository/cartao"
)

type MockCatalogoCartaoRepository struct {
	mock.Mock
}

func (mock *MockCatalogoCartaoRepository) Todos() ([]cartao.CatalogoCartaoMapper, error) {
	args := mock.Called()
	result := args.Get(0)
	return result.([]cartao.CatalogoCartaoMapper), args.Error(1)
}
