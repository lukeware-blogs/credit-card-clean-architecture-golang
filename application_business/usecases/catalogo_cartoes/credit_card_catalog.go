package catalogo_cartoes

type CreditCardCatalog struct {
	CardFlag string  `json:"card_flag,omitempty"`
	CardName string  `json:"card_name,omitempty"`
	Annuity  float64 `json:"annuity,omitempty"`
}
