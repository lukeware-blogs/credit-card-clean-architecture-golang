package catalogo_cartoes

import (
	"github.com/stretchr/testify/assert"
	repositoryCartao "gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/frameworks_drivers/repository/cartao"
	"testing"
)

func Test_devera_bucar_o_catalogo_de_cartoes(t *testing.T) {
	t.Parallel()
	assertions := assert.New(t)
	catalogoCartaoRepository := new(MockCatalogoCartaoRepository)
	catalogoCartaoPresenter := new(MockCatalogoCartaoPresenter)
	iCatalogoCartaoInteractor := NewCatalogoCartaoInteractor(catalogoCartaoRepository, catalogoCartaoPresenter)

	catalogoCartaoRepository.On("Todos").Return([]repositoryCartao.CatalogoCartaoMapper{
		{
			Anuidade: 4.99,
			Nome:     "Platinum Gold",
			Bandeira: repositoryCartao.VISA_CARD,
		},
	}, nil)

	catalogoCartaoPresenter.On("Sucesso").Return([]CreditCardCatalog{
		{
			Annuity:  4.99,
			CardFlag: repositoryCartao.VISA_CARD,
			CardName: "Platinum Gold",
		},
	})

	todos, err := iCatalogoCartaoInteractor.Todos()

	assertions.NoError(err)
	assertions.NotNil(todos)
	assertions.True(len(todos) > 0)
	assertions.Equal(4.99, todos[0].Annuity)
	assertions.Equal(repositoryCartao.VISA_CARD, todos[0].CardFlag)
	assertions.Equal("Platinum Gold", todos[0].CardName)

}
