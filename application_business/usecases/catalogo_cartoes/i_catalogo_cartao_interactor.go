package catalogo_cartoes

type ICatalogoCartaoInteractor interface {
	Todos() ([]CreditCardCatalog, error)
}
