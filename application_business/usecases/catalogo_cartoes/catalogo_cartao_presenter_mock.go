package catalogo_cartoes

import (
	"github.com/stretchr/testify/mock"
	repositoryCartao "gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/frameworks_drivers/repository/cartao"
)

type MockCatalogoCartaoPresenter struct {
	mock.Mock
}

func (mock MockCatalogoCartaoPresenter) Sucesso(todos []repositoryCartao.CatalogoCartaoMapper) []CreditCardCatalog {
	args := mock.Called()
	result := args.Get(0)
	return result.([]CreditCardCatalog)
}
