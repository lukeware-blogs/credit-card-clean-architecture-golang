package catalogo_cartoes

import (
	repositoryCartao "gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/frameworks_drivers/repository/cartao"
)

type catalogoCartaoInteractor struct {
	catalogoCartaoRepository repositoryCartao.ICatalogoCartaoRepository
	catalogoCartaoPresenter  ICatalogoCartaoPresenter
}

func NewCatalogoCartaoInteractor(
	catalogoCartaoRepository repositoryCartao.ICatalogoCartaoRepository,
	catalogoCartaoPresenter ICatalogoCartaoPresenter,
) ICatalogoCartaoInteractor {
	return catalogoCartaoInteractor{
		catalogoCartaoRepository: catalogoCartaoRepository,
		catalogoCartaoPresenter:  catalogoCartaoPresenter,
	}
}

func (c catalogoCartaoInteractor) Todos() ([]CreditCardCatalog, error) {
	todos, err := c.catalogoCartaoRepository.Todos()
	if err != nil {
		return []CreditCardCatalog{}, err
	}
	return c.catalogoCartaoPresenter.Sucesso(todos), nil
}
