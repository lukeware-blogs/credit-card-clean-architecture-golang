package catalogo_cartoes

import (
	repositoryCartao "gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/frameworks_drivers/repository/cartao"
)

type ICatalogoCartaoPresenter interface {
	Sucesso(todos []repositoryCartao.CatalogoCartaoMapper) []CreditCardCatalog
}
