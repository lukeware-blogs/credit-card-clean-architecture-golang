package cliente

type DadosClienteRequest struct {
	Tipo      string
	Documento string
	Email     string
	Telefone  string
}
