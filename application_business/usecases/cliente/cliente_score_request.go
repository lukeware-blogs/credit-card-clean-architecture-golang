package cliente

type ClienteScoreRequest struct {
	Tipo       string
	Documento  string
	Email      string
	Telefone   string
	Estrageiro bool
}
