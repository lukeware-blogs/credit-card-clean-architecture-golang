package gerador_cartao

import "time"

type CartaoClienteRequest struct {
	Tipo        string
	Documento   string
	Email       string
	Telefone    string
	Cartao      CartaoResponse
	DataCriacao time.Time
}
