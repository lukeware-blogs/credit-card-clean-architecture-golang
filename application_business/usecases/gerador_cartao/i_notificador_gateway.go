package gerador_cartao

type INotificadorGateway interface {
	Notificar(titular string, email string, mensagem string, titulo string) error
}
