package gerador_cartao

import (
	"github.com/stretchr/testify/mock"
)

type MockNotificadorGateway struct {
	mock.Mock
}

func (mock *MockNotificadorGateway) Notificar(titular string, email string, mensagem string, titulo string) error {
	args := mock.Called()
	return args.Error(0)
}
