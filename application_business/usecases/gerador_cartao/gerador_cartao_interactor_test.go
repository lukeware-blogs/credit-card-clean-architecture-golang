package gerador_cartao

import (
	"errors"
	"github.com/stretchr/testify/assert"
	entityCliente "gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/enterprise_business/entities/cliente"
	"testing"
)

func Test_devera_gerar_cartao_com_exito(t *testing.T) {
	t.Parallel()

	assertions := assert.New(t)

	gerardorCartaoGateway := new(MockGeradorCartaoGateway)
	notificadorGateway := new(MockNotificadorGateway)
	geradorCartaoInteractor := NewGeradorCartao(gerardorCartaoGateway, notificadorGateway)

	gerardorCartaoGateway.On("Gerar").Return(nil)
	notificadorGateway.On("Notificar").Return(nil)

	err := geradorCartaoInteractor.Gerar(CartaoClienteRequest{
		Cartao: CartaoResponse{
			Titular:       "Diego Morais",
			DiaVencimanto: 1,
			Limite:        1500.0,
		},
		Email:     "diego.morais@gmail.com",
		Telefone:  "+55 9 9999-9999",
		Documento: "999.999.999-99",
		Tipo:      entityCliente.PJ,
	})

	assertions.NoError(err)

}

func Test_devera_simular_error_ao_gerar_cartao(t *testing.T) {
	t.Parallel()

	assertions := assert.New(t)

	gerardorCartaoGateway := new(MockGeradorCartaoGateway)
	notificadorGateway := new(MockNotificadorGateway)
	geradorCartaoInteractor := NewGeradorCartao(gerardorCartaoGateway, notificadorGateway)

	gerardorCartaoGateway.On("Gerar").Return(errors.New("ops, houve uma falha no sistema. tente mais tarde"))

	err := geradorCartaoInteractor.Gerar(CartaoClienteRequest{
		Cartao: CartaoResponse{
			Titular:       "Diego Morais",
			DiaVencimanto: 1,
			Limite:        1500.0,
		},
		Email:     "diego.morais@gmail.com",
		Telefone:  "+55 9 9999-9999",
		Documento: "999.999.999-99",
		Tipo:      entityCliente.PJ,
	})

	assertions.EqualError(err, "ops, houve uma falha no sistema. tente mais tarde")

}
func Test_devera_simular_error_ao_notificar(t *testing.T) {
	t.Parallel()

	assertions := assert.New(t)

	gerardorCartaoGateway := new(MockGeradorCartaoGateway)
	notificadorGateway := new(MockNotificadorGateway)
	geradorCartaoInteractor := NewGeradorCartao(gerardorCartaoGateway, notificadorGateway)

	gerardorCartaoGateway.On("Gerar").Return(nil)
	notificadorGateway.On("Notificar").Return(errors.New("ops, houve uma falha no sistema. tente mais tarde"))

	err := geradorCartaoInteractor.Gerar(CartaoClienteRequest{
		Cartao: CartaoResponse{
			Titular:       "Diego Morais",
			DiaVencimanto: 1,
			Limite:        1500.0,
		},
		Email:     "diego.morais@gmail.com",
		Telefone:  "+55 9 9999-9999",
		Documento: "999.999.999-99",
		Tipo:      entityCliente.PJ,
	})

	assertions.EqualError(err, "ops, houve uma falha no sistema. tente mais tarde")

}
