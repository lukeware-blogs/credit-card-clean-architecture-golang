package gerador_cartao

import (
	"github.com/stretchr/testify/mock"
)

type MockGeradorCartaoGateway struct {
	mock.Mock
}

func (mock *MockGeradorCartaoGateway) Gerar(request CartaoClienteRequest) error {
	args := mock.Called()
	return args.Error(0)
}
