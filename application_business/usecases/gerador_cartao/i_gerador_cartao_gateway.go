package gerador_cartao

type IGeradorCartaoGateway interface {
	Gerar(request CartaoClienteRequest) error
}
