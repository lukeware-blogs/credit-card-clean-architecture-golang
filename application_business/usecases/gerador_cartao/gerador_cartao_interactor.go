package gerador_cartao

type geradorCartaoInteractor struct {
	geradorCartaoGateway IGeradorCartaoGateway
	notificadorGateway   INotificadorGateway
}

func NewGeradorCartao(geradorCartaoGateway IGeradorCartaoGateway,
	notificadorGateway INotificadorGateway) IGeradorCartaoInteractor {
	return geradorCartaoInteractor{geradorCartaoGateway: geradorCartaoGateway, notificadorGateway: notificadorGateway}
}

func (g geradorCartaoInteractor) Gerar(request CartaoClienteRequest) error {
	err := g.geradorCartaoGateway.Gerar(request)
	if err != nil {
		return err
	}
	errEmail := g.notificadorGateway.Notificar(request.Cartao.Titular, request.Email, "solicitação do novo cartão de crédito realizada com êxito", "Parabéns pelo seu novo cartão")
	if errEmail != nil {
		return errEmail
	}
	return nil
}
