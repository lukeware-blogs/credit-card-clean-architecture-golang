package gerador_cartao

type CartaoResponse struct {
	Titular       string
	DiaVencimanto int
	Limite        float64
	Serie         string
	Cvc           string
	NomeCartao    string
	Bandeira      string
	Anuidade      float64
}
