package gerador_cartao

type IGeradorCartaoInteractor interface {
	Gerar(request CartaoClienteRequest) error
}
