package score

import (
	usecaseCliente "gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/application_business/usecases/cliente"
)

type IScoreInteractor interface {
	ObterScore(request usecaseCliente.ClienteScoreRequest) (ScoreResponse, error)
}
