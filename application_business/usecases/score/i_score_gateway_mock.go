package score

import (
	"github.com/stretchr/testify/mock"
)

type MockScoreGateway struct {
	mock.Mock
}

func (mock *MockScoreGateway) ObterScore(request ScoreRequest) (int, error) {
	args := mock.Called()
	result := args.Get(0)
	return result.(int), args.Error(1)
}
