package score

type IScoreGateway interface {
	ObterScore(request ScoreRequest) (int, error)
}
