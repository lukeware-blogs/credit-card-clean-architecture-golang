package score

import (
	"errors"
	usecaseCliente "gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/application_business/usecases/cliente"
	entityScore "gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/enterprise_business/entities/score"
)

type scoreInteractor struct {
	scoreGateway IScoreGateway
}

func NewScoreInteractor(scoreGateway IScoreGateway) IScoreInteractor {
	return scoreInteractor{
		scoreGateway: scoreGateway,
	}
}

func (s scoreInteractor) ObterScore(request usecaseCliente.ClienteScoreRequest) (ScoreResponse, error) {

	if request.Estrageiro {
		return ScoreResponse{}, errors.New("não temos informações do score desse cliente")
	}

	score, scoreError := s.scoreGateway.ObterScore(ScoreRequest{
		Documento:  request.Documento,
		TipoPessoa: request.Tipo,
	})
	if scoreError != nil {
		return ScoreResponse{}, errors.New("ops, houve uma falha no sistema. tente mais tarde")
	}

	newScore := entityScore.NewScore(score)
	err := newScore.EValido()
	if err != nil {
		return ScoreResponse{}, err
	}

	limit := 0.0

	switch newScore.Nivel() {
	case 1:
		limit = 10000.0
	case 2:
		limit = 5000.0
	case 3:
		limit = 1000.0
	}

	return ScoreResponse{Nivel: newScore.Nivel(), Limite: limit}, nil
}
