package score

import (
	"errors"
	"github.com/stretchr/testify/assert"
	useCaseCliente "gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/application_business/usecases/cliente"
	entityCliente "gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/enterprise_business/entities/cliente"
	"testing"
)

func Test_deverar_validar_score_cliente_nivel_1(t *testing.T) {
	t.Parallel()

	assertions := assert.New(t)

	scoreGateway := new(MockScoreGateway)
	scoreInteractor := NewScoreInteractor(scoreGateway)

	scoreGateway.On("ObterScore").Return(1000, nil)

	scoreResponse, err := scoreInteractor.ObterScore(useCaseCliente.ClienteScoreRequest{
		Tipo:       entityCliente.PJ,
		Documento:  "999.999.999-99",
		Email:      "diego.morais@gmail.com",
		Estrageiro: false,
	})

	assertions.NoError(err)
	assertions.Equal(1, scoreResponse.Nivel)
}

func Test_deverar_validar_score_cliente_nivel_5(t *testing.T) {
	t.Parallel()

	assertions := assert.New(t)

	scoreGateway := new(MockScoreGateway)
	scoreInteractor := NewScoreInteractor(scoreGateway)

	scoreGateway.On("ObterScore").Return(150, nil)

	scoreResponse, err := scoreInteractor.ObterScore(useCaseCliente.ClienteScoreRequest{
		Tipo:       entityCliente.PJ,
		Documento:  "999.999.999-99",
		Email:      "diego.morais@gmail.com",
		Estrageiro: false,
	})

	assertions.NoError(err)
	assertions.Equal(5, scoreResponse.Nivel)
}

func Test_deverar_validar_score_cliente_estrageiro(t *testing.T) {
	t.Parallel()

	assertions := assert.New(t)

	scoreGateway := new(MockScoreGateway)
	scoreInteractor := NewScoreInteractor(scoreGateway)

	scoreResponse, err := scoreInteractor.ObterScore(useCaseCliente.ClienteScoreRequest{
		Tipo:       entityCliente.PJ,
		Documento:  "999.999.999-99",
		Email:      "diego.morais@gmail.com",
		Estrageiro: true,
	})

	assertions.EqualError(err, "não temos informações do score desse cliente")
	assertions.NotNil(scoreResponse)
}

func Test_deverar_validar_score_cliente_com_erro(t *testing.T) {
	t.Parallel()

	assertions := assert.New(t)

	scoreGateway := new(MockScoreGateway)
	scoreInteractor := NewScoreInteractor(scoreGateway)

	scoreGateway.On("ObterScore").Return(0, errors.New("erro ao buscar score"))

	scoreResponse, err := scoreInteractor.ObterScore(useCaseCliente.ClienteScoreRequest{
		Tipo:       entityCliente.PJ,
		Documento:  "999.999.999-99",
		Email:      "diego.morais@gmail.com",
		Estrageiro: false,
	})

	assertions.EqualError(err, "ops, houve uma falha no sistema. tente mais tarde")
	assertions.NotNil(scoreResponse)
}

func Test_deverar_validar_score_cliente_negativo(t *testing.T) {
	t.Parallel()

	assertions := assert.New(t)

	scoreGateway := new(MockScoreGateway)
	scoreInteractor := NewScoreInteractor(scoreGateway)

	scoreGateway.On("ObterScore").Return(-1000, nil)

	scoreResponse, err := scoreInteractor.ObterScore(useCaseCliente.ClienteScoreRequest{
		Tipo:       entityCliente.PJ,
		Documento:  "999.999.999-99",
		Email:      "diego.morais@gmail.com",
		Estrageiro: false,
	})

	assertions.EqualError(err, "pontos inválidos")
	assertions.NotNil(scoreResponse)
}
