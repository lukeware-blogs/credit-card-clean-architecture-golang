FROM golang:1.18.2-buster AS build
WORKDIR /app
COPY go.mod ./
COPY go.sum ./
#RUN go mod tidy
RUN go mod download
COPY application_business ./application_business
COPY enterprise_business ./enterprise_business
COPY frameworks_drivers ./frameworks_drivers
COPY graph ./graph
COPY interface_adapters ./interface_adapters
COPY gqlgen.yml ./
COPY tools.go ./
COPY main.go ./


ARG EMAIL_API_KEY
ARG EMAIL_ADMIN
ARG EMAIL_ADMIN_NAME
ARG EMAIL_SMTP_HOST
ARG EMAIL_SMTP_PORT
ARG DATABASE_POSTGRES_URL
ARG DATABASE_MONGODB_URL
ARG PORT_SERVER

ENV EMAIL_API_KEY=${EMAIL_API_KEY}
ENV EMAIL_ADMIN=${EMAIL_ADMIN}
ENV EMAIL_ADMIN_NAME=${EMAIL_ADMIN_NAME}
ENV EMAIL_SMTP_HOST=${EMAIL_SMTP_HOST}
ENV EMAIL_SMTP_PORT=${EMAIL_SMTP_PORT}
ENV DATABASE_POSTGRES_URL=${DATABASE_POSTGRES_URL}
ENV DATABASE_MONGODB_URL=${DATABASE_MONGODB_URL}
ENV PORT_SERVER=${PORT_SERVER}

RUN go build -o /cred-card-manager
##
## Deploy
##
FROM gcr.io/distroless/base-debian10
WORKDIR /
COPY --from=build /cred-card-manager /cred-card-manager
EXPOSE 1323
USER nonroot:nonroot
ENTRYPOINT ["/cred-card-manager"]
