package cartao

import (
	"gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/application_business/usecases/catalogo_cartoes"
)

type ICatalogoCartaoController interface {
	Todos() ([]catalogo_cartoes.CreditCardCatalog, error)
}
