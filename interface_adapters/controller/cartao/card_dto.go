package cartao

type CardDto struct {
	Document      string  `json:"document"`
	Email         string  `json:"email"`
	Phone         string  `json:"phone"`
	TypePerson    string  `json:"type_person"`
	Cardholder    string  `json:"cardholder"`
	ExpirationDay int     `json:"expiration_day"`
	CardFlag      string  `json:"card_flag"`
	Annuity       float64 `json:"annuity"`
	NameCard      string  `json:"name_card"`
}
