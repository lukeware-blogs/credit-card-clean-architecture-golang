package cartao

import (
	"gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/application_business/usecases/catalogo_cartoes"
)

type catalogoCartaoController struct {
	catalogoCartaoInteractor catalogo_cartoes.ICatalogoCartaoInteractor
}

func NewCatalogoCartaoController(catalogoCartaoInteractor catalogo_cartoes.ICatalogoCartaoInteractor) ICatalogoCartaoController {
	return catalogoCartaoController{catalogoCartaoInteractor: catalogoCartaoInteractor}
}

func (c catalogoCartaoController) Todos() ([]catalogo_cartoes.CreditCardCatalog, error) {
	return c.catalogoCartaoInteractor.Todos()
}
