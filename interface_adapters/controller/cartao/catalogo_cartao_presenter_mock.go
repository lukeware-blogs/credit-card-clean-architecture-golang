package cartao

import (
	"github.com/stretchr/testify/mock"
	"gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/application_business/usecases/catalogo_cartoes"
)

type MockCatalogoCartaoInteractor struct {
	mock.Mock
}

func (mock *MockCatalogoCartaoInteractor) Todos() ([]catalogo_cartoes.CreditCardCatalog, error) {
	args := mock.Called()
	result := args.Get(0)
	return result.([]catalogo_cartoes.CreditCardCatalog), args.Error(1)
}
