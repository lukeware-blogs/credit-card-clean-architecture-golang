package cartao

import (
	"github.com/stretchr/testify/mock"
	"gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/application_business/usecases/cartao"
	entityCartao "gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/enterprise_business/entities/cartao"
)

type MockCartaoInteractor struct {
	mock.Mock
}

func (mock MockCartaoInteractor) Criar(cartaoCliente entityCartao.ICartaoCliente) cartao.CartaoResponse {
	args := mock.Called()
	result := args.Get(0)
	return result.(cartao.CartaoResponse)
}
