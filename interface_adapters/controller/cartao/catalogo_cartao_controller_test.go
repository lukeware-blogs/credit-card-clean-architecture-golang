package cartao

import (
	"github.com/stretchr/testify/assert"
	cartao2 "gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/application_business/usecases/catalogo_cartoes"
	"gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/frameworks_drivers/repository/cartao"
	"testing"
)

func Test_devera_buscar_todos_os_cartoes_catalogados(t *testing.T) {
	t.Parallel()

	assertions := assert.New(t)

	iCatalogoCartaoInteractor := new(MockCatalogoCartaoInteractor)

	iCatalogoCartaoController := NewCatalogoCartaoController(iCatalogoCartaoInteractor)

	iCatalogoCartaoInteractor.On("Todos").Return([]cartao2.CreditCardCatalog{
		{
			CardName: "Platinum Golden",
			CardFlag: cartao.VISA_CARD,
			Annuity:  4.99,
		},
	}, nil)

	creditCardCatalogs, err := iCatalogoCartaoController.Todos()

	assertions.NoError(err)
	assertions.NotNil(creditCardCatalogs)
	assertions.True(len(creditCardCatalogs) > 0)
	assertions.Equal(cartao.VISA_CARD, creditCardCatalogs[0].CardFlag)
	assertions.Equal("Platinum Golden", creditCardCatalogs[0].CardName)
	assertions.Equal(4.99, creditCardCatalogs[0].Annuity)
}
