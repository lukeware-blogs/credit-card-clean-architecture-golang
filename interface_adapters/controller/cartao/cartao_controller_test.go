package cartao

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/application_business/usecases/cartao"
	"testing"
)

func Test_devera_criar_cartao_com_sucesso(t *testing.T) {
	t.Parallel()

	assertions := assert.New(t)

	cartaoInteractor := new(MockCartaoInteractor)

	iCartaoController := NewCartaoController(cartaoInteractor)

	cartaoInteractor.On("Criar").Return(cartao.CartaoResponse{
		Success: "parabéns Diego Morais! seu novo cartão foi gerado, confira sua caixa de entreda",
	})

	cartaoResponse := iCartaoController.Criar(CardDto{
		Email:         "diego.morais@gmail.com",
		Cardholder:    "Diego Morais",
		Document:      "999.999.999-99",
		ExpirationDay: 1,
		Phone:         "+55 19 9 9999-9999",
		TypePerson:    "PF",
	})

	assertions.NotNil(cartaoResponse)
	assertions.Equal("parabéns Diego Morais! seu novo cartão foi gerado, confira sua caixa de entreda", cartaoResponse.Success)
}

func Test_devera_tentar_criar_cartao_com_erro(t *testing.T) {
	t.Parallel()

	assertions := assert.New(t)

	cartaoInteractor := new(MockCartaoInteractor)

	iCartaoController := NewCartaoController(cartaoInteractor)

	cartaoInteractor.On("Criar").Return(cartao.CartaoResponse{
		Error: "desculpe, não foi possivel criar o seu cartão, tente novamente em 6 meses",
	})

	cartaoResponse := iCartaoController.Criar(CardDto{
		Email:         "diego.morais@gmail.com",
		Cardholder:    "Diego Morais",
		Document:      "999.999.999-99",
		ExpirationDay: 1,
		Phone:         "+55 19 9 9999-9999",
		TypePerson:    "PF",
	})

	assertions.NotNil(cartaoResponse)
	assertions.Equal("", cartaoResponse.Success)
	assertions.Equal("desculpe, não foi possivel criar o seu cartão, tente novamente em 6 meses", cartaoResponse.Error)
}
