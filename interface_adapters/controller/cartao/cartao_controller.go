package cartao

import (
	usecaseCartao "gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/application_business/usecases/cartao"
	entityCartaoCliente "gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/enterprise_business/entities/cartao"
	entityCliente "gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/enterprise_business/entities/cliente"
	"gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/frameworks_drivers/utility"
)

type cartaoController struct {
	cartaoInteractor usecaseCartao.ICartaoInteractor
	validator        utility.IValidator
}

func NewCartaoController(cartaoInteractor usecaseCartao.ICartaoInteractor) ICartaoController {
	return cartaoController{
		cartaoInteractor: cartaoInteractor,
		validator:        utility.NewValidator(),
	}
}

func (c cartaoController) Criar(cardDto CardDto) usecaseCartao.CartaoResponse {
	cliente := entityCliente.NewCliente(cardDto.TypePerson, cardDto.Document, cardDto.Email, cardDto.Phone, c.validator)
	cartao := entityCartaoCliente.NewCartao(cardDto.Cardholder, cardDto.ExpirationDay, cardDto.NameCard, cardDto.Annuity, cardDto.CardFlag, c.validator)
	cartaoCliente := entityCartaoCliente.NewCartaoCliente(cartao, cliente, c.validator)
	return c.cartaoInteractor.Criar(cartaoCliente)
}
