package cartao

import usecaseCartao "gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/application_business/usecases/cartao"

type ICartaoController interface {
	Criar(cardDto CardDto) usecaseCartao.CartaoResponse
}
