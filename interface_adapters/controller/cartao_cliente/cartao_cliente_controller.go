package cartao_cliente

import (
	"gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/application_business/usecases/cartao_cliente"
	graphModel "gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/graph/model"
)

type cartaoClienteController struct {
	cartaoClienteInteractor cartao_cliente.ICartaoClienteInteractor
}

func NewCartaoClienteController(cartaoClienteInteractor cartao_cliente.ICartaoClienteInteractor) ICartaoClienteController {
	return cartaoClienteController{
		cartaoClienteInteractor: cartaoClienteInteractor,
	}
}

func (c cartaoClienteController) Todos() ([]*graphModel.CustomerCreditCard, error) {
	return c.cartaoClienteInteractor.Todos()
}
func (c cartaoClienteController) PorDocumento(documento string) ([]*graphModel.CustomerCreditCard, error) {
	return c.cartaoClienteInteractor.PorDocumento(documento)
}
