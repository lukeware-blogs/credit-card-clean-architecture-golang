package cartao

import (
	"errors"
	"github.com/stretchr/testify/assert"
	"gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/application_business/usecases/gerador_cartao"
	"gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/enterprise_business/entities/cliente"
	"testing"
)

func Test_devera_gerar_cartao_com_sucesso(t *testing.T) {
	t.Parallel()

	assertions := assert.New(t)

	cartaoClienteRepository := new(MockCartaoClienteRepository)
	geradorCartaoGateway := NewGeradorCartaoGateway(cartaoClienteRepository)

	cartaoClienteRepository.On("Salvar").Return("81dc7794-d217-455f-b2c0-51a8848fdfe5", nil)

	err := geradorCartaoGateway.Gerar(gerador_cartao.CartaoClienteRequest{
		Tipo:      cliente.PF,
		Documento: "999.999.999-99",
		Email:     "diegomorais@gmail.com",
		Telefone:  "+55 (19) 9 9999-9999",
		Cartao: gerador_cartao.CartaoResponse{
			DiaVencimanto: 1,
			Titular:       "Diego Morais",
			Limite:        15000.0,
		},
	})

	assertions.NoError(err)
}

func Test_devera_tentar_gerar_cartao_com_error(t *testing.T) {
	t.Parallel()

	assertions := assert.New(t)

	cartaoClienteRepository := new(MockCartaoClienteRepository)
	geradorCartaoGateway := NewGeradorCartaoGateway(cartaoClienteRepository)

	cartaoClienteRepository.On("Salvar").Return("", errors.New("erro a conectar com o banco"))

	err := geradorCartaoGateway.Gerar(gerador_cartao.CartaoClienteRequest{
		Tipo:      cliente.PF,
		Documento: "999.999.999-99",
		Email:     "diegomorais@gmail.com",
		Telefone:  "+55 (19) 9 9999-9999",
		Cartao: gerador_cartao.CartaoResponse{
			DiaVencimanto: 1,
			Titular:       "Diego Morais",
			Limite:        15000.0,
		},
	})

	assertions.EqualError(err, "ops, houve uma falha no sistema. tente mais tarde")
}
