package cartao

import (
	"github.com/stretchr/testify/mock"
	usecaseCartao "gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/application_business/usecases/gerador_cartao"
)

type MockCartaoClienteRepository struct {
	mock.Mock
}

func (mock *MockCartaoClienteRepository) Salvar(request usecaseCartao.CartaoClienteRequest) (string, error) {
	args := mock.Called()
	result := args.Get(0)
	return result.(string), args.Error(1)
}

func (mock *MockCartaoClienteRepository) PorDocumento(documento string) ([]usecaseCartao.CartaoClienteResponse, error) {
	args := mock.Called()
	result := args.Get(0)
	return result.([]usecaseCartao.CartaoClienteResponse), args.Error(1)
}

func (mock *MockCartaoClienteRepository) Todos() ([]usecaseCartao.CartaoClienteResponse, error) {
	args := mock.Called()
	result := args.Get(0)
	return result.([]usecaseCartao.CartaoClienteResponse), args.Error(1)
}
