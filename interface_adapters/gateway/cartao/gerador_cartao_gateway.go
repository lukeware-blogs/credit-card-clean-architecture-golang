package cartao

import (
	"errors"
	"gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/application_business/usecases/gerador_cartao"
	"gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/frameworks_drivers/repository/cartao"
	"gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/frameworks_drivers/utility"
	"time"
)

type geradorCartaoGateway struct {
	cartaoClienteRepository cartao.ICartaoClienteRepository
	geradorNumeroCartao     utility.IGeradorNumeroCartao
}

func NewGeradorCartaoGateway(cartaoClienteRepository cartao.ICartaoClienteRepository) gerador_cartao.IGeradorCartaoGateway {
	return geradorCartaoGateway{
		cartaoClienteRepository: cartaoClienteRepository,
		geradorNumeroCartao:     utility.NewGeradorNumeroCartao(),
	}
}

func (g geradorCartaoGateway) Gerar(cartaoClienteRequest gerador_cartao.CartaoClienteRequest) error {
	serie := g.geradorNumeroCartao.GerarNumeroSerie()
	cvc := g.geradorNumeroCartao.GerarCvc()
	cartaoClienteRequest.Cartao.Serie = serie
	cartaoClienteRequest.Cartao.Cvc = cvc
	cartaoClienteRequest.DataCriacao = time.Now()
	_, error := g.cartaoClienteRepository.Salvar(cartaoClienteRequest)
	if error != nil {
		return errors.New("ops, houve uma falha no sistema. tente mais tarde")
	}
	return nil
}
