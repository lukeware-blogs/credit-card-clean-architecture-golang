package score

import (
	usecaseScore "gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/application_business/usecases/score"
	"gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/frameworks_drivers/repository/score"
)

type scoreGateway struct {
	scoreRepository score.IScoreRepository
}

func NewScoreGateway(scoreRepository score.IScoreRepository) usecaseScore.IScoreGateway {
	return scoreGateway{
		scoreRepository: scoreRepository,
	}
}

func (s scoreGateway) ObterScore(request usecaseScore.ScoreRequest) (int, error) {
	score, err := s.scoreRepository.ObterScore(request.Documento, request.TipoPessoa)
	return score.Score, err
}
