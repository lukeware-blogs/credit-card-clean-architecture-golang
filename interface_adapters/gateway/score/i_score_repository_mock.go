package score

import (
	"github.com/stretchr/testify/mock"
	"gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/frameworks_drivers/repository/score"
)

type MockIScoreRepository struct {
	mock.Mock
}

func (mock *MockIScoreRepository) ObterScore(documento string, tipoDocumento string) (*score.ScoreMapper, error) {
	args := mock.Called()
	result := args.Get(0)
	mapper := result.(score.ScoreMapper)
	return &mapper, args.Error(1)
}
