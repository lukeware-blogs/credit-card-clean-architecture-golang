package score

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/application_business/usecases/score"
	"gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/enterprise_business/entities/cliente"
	score2 "gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/frameworks_drivers/repository/score"
	"testing"
)

func Test_devera_consultar_score_do_cliente(t *testing.T) {
	t.Parallel()

	assertions := assert.New(t)

	iScoreRepository := new(MockIScoreRepository)
	scoreGateway := NewScoreGateway(iScoreRepository)

	iScoreRepository.On("ObterScore").Return(score2.ScoreMapper{
		Score:      1000,
		Documento:  "999.999.999-99",
		TipoPessoa: cliente.PF,
	}, nil)

	score, err := scoreGateway.ObterScore(score.ScoreRequest{
		Documento:  "999.999.999-99",
		TipoPessoa: cliente.PF,
	})

	assertions.NoError(err)
	assertions.Equal(1000, score)

}
