package notificador

import (
	"github.com/stretchr/testify/mock"
	"gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/frameworks_drivers/notificator"
)

type MockNotificadorMessage struct {
	mock.Mock
}

func (mock *MockNotificadorMessage) Push(request notificator.NotificadorRequest) error {
	args := mock.Called()
	return args.Error(0)
}
