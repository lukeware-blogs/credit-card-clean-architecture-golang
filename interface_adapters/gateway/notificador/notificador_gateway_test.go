package notificador

import (
	"errors"
	"github.com/stretchr/testify/assert"
	"testing"
)

func Test_devera_enviar_notificacoes_com_sucesso(t *testing.T) {
	t.Parallel()

	assertions := assert.New(t)

	mockNotificadorMessage := new(MockNotificadorMessage)
	notificadorGateway := NewNotificadorGateway(mockNotificadorMessage)

	mockNotificadorMessage.On("Push").Return(nil)

	err := notificadorGateway.Notificar("Diego Morais", "diego.morais@gmail.com", "solicitação do novo cartão de crédito realizada com êxito", "Parabéns pelo seu novo cartão")

	assertions.NoError(err)

}

func Test_devera_simular_erro_ao_notificar(t *testing.T) {
	t.Parallel()

	assertions := assert.New(t)

	mockNotificadorMessage := new(MockNotificadorMessage)
	notificadorGateway := NewNotificadorGateway(mockNotificadorMessage)

	mockNotificadorMessage.On("Push").Return(errors.New("erro ao realizar o push"))

	err := notificadorGateway.Notificar("Diego Morais", "diego.morais@gmail.com", "solicitação do novo cartão de crédito realizada com êxito", "Parabéns pelo seu novo cartão")

	assertions.EqualError(err, "ops, houve uma falha no sistema. tente mais tarde")

}
