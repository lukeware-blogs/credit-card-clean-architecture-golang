package notificador

import (
	"errors"
	usecaseGeradorCartao "gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/application_business/usecases/gerador_cartao"
	"gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/frameworks_drivers/notificator"
)

type notificadorGateway struct {
	notificadorMessage notificator.INotificadorMessage
}

func NewNotificadorGateway(notificadorMessage notificator.INotificadorMessage) usecaseGeradorCartao.INotificadorGateway {
	return notificadorGateway{notificadorMessage: notificadorMessage}
}

func (n notificadorGateway) Notificar(titular string, email string, mensagem string, titulo string) error {
	err := n.notificadorMessage.Push(notificator.NotificadorRequest{
		Titulo:       titulo,
		Email:        email,
		Mensagem:     mensagem,
		Destinatario: titular,
	})

	if err != nil {
		return errors.New("ops, houve uma falha no sistema. tente mais tarde")
	}

	return nil
}
