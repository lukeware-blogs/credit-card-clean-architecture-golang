package antifraud

import (
	"gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/enterprise_business/entities/cartao"
	"gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/frameworks_drivers/repository/antifraud"
)

type antiFraudGateway struct {
	antiFraudRepository antifraud.IAntiFraudRepository
}

func NewAntiFraudGateway(antiFraudRepository antifraud.IAntiFraudRepository) IAntiFraudGateway {
	return antiFraudGateway{
		antiFraudRepository: antiFraudRepository,
	}
}

func (a antiFraudGateway) ClienteFraudador(documento string) (string, error) {
	clienteFraudadorResponse, err := a.antiFraudRepository.ClienteFraudador(documento)
	if err != nil {
		return cartao.CLIENTE_NAO_REGISTRADO, err
	}
	return clienteFraudadorResponse.Status, nil
}
