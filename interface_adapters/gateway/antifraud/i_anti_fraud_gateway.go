package antifraud

type IAntiFraudGateway interface {
	ClienteFraudador(documento string) (string, error)
}
