package presenters

import (
	"gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/application_business/usecases/catalogo_cartoes"
	repositoryCartao "gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/frameworks_drivers/repository/cartao"
)

type catalogoCartaoPresenter struct {
}

func NewCatalogoCartaoPresenter() catalogo_cartoes.ICatalogoCartaoPresenter {
	return catalogoCartaoPresenter{}
}

func (c catalogoCartaoPresenter) Sucesso(todos []repositoryCartao.CatalogoCartaoMapper) []catalogo_cartoes.CreditCardCatalog {
	catalogs := []catalogo_cartoes.CreditCardCatalog{}
	for _, item := range todos {
		catalogs = append(catalogs, catalogo_cartoes.CreditCardCatalog{
			CardFlag: item.Bandeira,
			CardName: item.Nome,
			Annuity:  item.Anuidade,
		})
	}
	return catalogs
}
