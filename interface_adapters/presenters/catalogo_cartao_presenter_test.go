package presenters

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/frameworks_drivers/repository/cartao"
	"testing"
)

func Test_devera_apresentar_catalogo_de_cartao_com_sucesso(t *testing.T) {
	t.Parallel()

	assertions := assert.New(t)

	iCatalogoCartaoPresenter := NewCatalogoCartaoPresenter()

	todos := iCatalogoCartaoPresenter.Sucesso([]cartao.CatalogoCartaoMapper{
		{
			Anuidade: 4.99,
			Nome:     "Platino Golden",
			Bandeira: cartao.MASTER_CARD,
		},
		{
			Anuidade: 5.99,
			Nome:     "Platino Premium",
			Bandeira: cartao.VISA_CARD,
		},
	})

	assertions.NotNil(todos)
	assertions.True(len(todos) == 2)
	assertions.Equal(cartao.MASTER_CARD, todos[0].CardFlag)
	assertions.Equal("Platino Golden", todos[0].CardName)
	assertions.Equal(4.99, todos[0].Annuity)
}
