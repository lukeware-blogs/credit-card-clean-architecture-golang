package presenters

import (
	usecaseCartao "gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/application_business/usecases/cartao"
)

type CartaoPresenter struct {
}

func NewCartaoPresenter() usecaseCartao.ICartaoPresenter {
	return CartaoPresenter{}
}

func (c CartaoPresenter) Falha(err error) usecaseCartao.CartaoResponse {
	return usecaseCartao.CartaoResponse{
		Error: err.Error(),
	}
}

func (c CartaoPresenter) Sucesso(nomeCliente string) usecaseCartao.CartaoResponse {
	return usecaseCartao.CartaoResponse{
		Success: "parabéns " + nomeCliente + "! seu novo cartão foi gerado, confira sua caixa de entreda",
	}
}
