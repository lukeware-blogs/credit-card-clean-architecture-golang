package presenters

import (
	"errors"
	"github.com/stretchr/testify/assert"
	"testing"
)

func Test_devera_apresentar_retorno_de_sucesso(t *testing.T) {
	t.Parallel()

	assertions := assert.New(t)

	cartaoPresenter := NewCartaoPresenter()

	response := cartaoPresenter.Sucesso("Diego Morais")

	assertions.NotNil(response)
	assertions.Equal("parabéns Diego Morais! seu novo cartão foi gerado, confira sua caixa de entreda", response.Success)
}

func Test_devera_apresentar_retorno_com_falha(t *testing.T) {
	t.Parallel()

	assertions := assert.New(t)

	cartaoPresenter := NewCartaoPresenter()

	response := cartaoPresenter.Falha(errors.New("desculpe, não foi possivel criar o seu cartão, tente novamente em 6 meses"))

	assertions.NotNil(response)
	assertions.Equal("desculpe, não foi possivel criar o seu cartão, tente novamente em 6 meses", response.Error)
}
