package presenters

import (
	"gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/application_business/usecases/cartao_cliente"
	"gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/application_business/usecases/gerador_cartao"
	graphModel "gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/graph/model"
)

type cartaoClientePresenter struct {
}

func NewCartaoClientePresenter() cartao_cliente.ICartaoClientePresenter {
	return cartaoClientePresenter{}
}

func (c cartaoClientePresenter) Success(todos []gerador_cartao.CartaoClienteResponse) []*graphModel.CustomerCreditCard {

	var creditCards []*graphModel.CustomerCreditCard
	for _, item := range todos {
		creditCards = append(creditCards, &graphModel.CustomerCreditCard{
			Type:     item.Tipo,
			Phone:    item.Telefone,
			Document: item.Documento,
			Email:    item.Email,
			CredCard: &graphModel.CardCredit{
				ExpirationDay: item.Cartao.DiaVencimanto,
				CardName:      item.Cartao.NomeCartao,
				Annuity:       item.Cartao.Anuidade,
				Series:        item.Cartao.Serie,
				Cardholder:    item.Cartao.Titular,
				CardFlag:      item.Cartao.Bandeira,
				Cvc:           item.Cartao.Cvc,
				Limit:         item.Cartao.Limite,
			},
		})
	}
	return creditCards
}
