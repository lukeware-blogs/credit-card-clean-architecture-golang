package properties

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func Test_devera_carregar_as_propriedade_com_sucesso(t *testing.T) {
	t.Parallel()

	assertions := assert.New(t)

	newProperties := NewProperties("$GOPATH/.env-test")
	load := newProperties.Load()

	assertions.Equal("chave_de_acesso", load.ApiKey)
	assertions.Equal("empresa@gmail.com", load.EmailAdmin)
	assertions.Equal("smtp.sendgrid.net", load.SmtpHost)
	assertions.Equal("587", load.SmtpPort)
	assertions.Equal("Empresa", load.NameAdmin)
	assertions.Equal("mongodb://admin:admin123@127.0.0.1:27017", load.DataBaseMongoUrl)
	assertions.Equal("host=127.0.0.1 port=5432 user=postgres password=admin123 dbname=labsit sslmode=disable", load.DataBasePostgresUrl)

}

func Test_devera_simular_arquivo_nao_encontrado(t *testing.T) {
	t.Parallel()

	assertions := assert.New(t)

	newProperties := NewProperties("$GOPATH/.env-test0")
	load := newProperties.Load()

	assertions.Equal("", load.ApiKey)
	assertions.Equal("", load.EmailAdmin)
	assertions.Equal("", load.SmtpHost)
	assertions.Equal("", load.SmtpPort)
	assertions.Equal("", load.NameAdmin)
	assertions.Equal("", load.DataBaseMongoUrl)
	assertions.Equal("", load.DataBasePostgresUrl)

}
