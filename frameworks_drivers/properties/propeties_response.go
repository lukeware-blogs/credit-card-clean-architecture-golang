package properties

type PropertiesResponse struct {
	ApiKey              string
	EmailAdmin          string
	NameAdmin           string
	SmtpHost            string
	SmtpPort            string
	DataBasePostgresUrl string
	DataBaseMongoUrl    string
	PortServer          string
}
