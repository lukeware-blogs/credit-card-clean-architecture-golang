package properties

type IProperties interface {
	Load() PropertiesResponse
}
