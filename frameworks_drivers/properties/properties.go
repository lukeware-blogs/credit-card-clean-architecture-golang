package properties

import (
	"github.com/joho/godotenv"
	"log"
	"os"
)

type properties struct {
	path string
}

func NewProperties(path string) IProperties {
	return properties{path: path}
}

func (p properties) Load() PropertiesResponse {
	err := godotenv.Load(os.ExpandEnv(p.path)) //"$GOPATH/.env"
	if err != nil {
		log.Println(err)
	}
	return PropertiesResponse{
		EmailAdmin:          os.Getenv("EMAIL_ADMIN"),
		NameAdmin:           os.Getenv("EMAIL_ADMIN_NAME"),
		ApiKey:              os.Getenv("EMAIL_API_KEY"),
		SmtpHost:            os.Getenv("EMAIL_SMTP_HOST"),
		SmtpPort:            os.Getenv("EMAIL_SMTP_PORT"),
		DataBasePostgresUrl: os.Getenv("DATABASE_POSTGRES_URL"),
		DataBaseMongoUrl:    os.Getenv("DATABASE_MONGODB_URL"),
		PortServer:          os.Getenv("PORT_SERVER"),
	}
}
