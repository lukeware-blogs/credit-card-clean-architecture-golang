package antifraud

type ClienteFraudadorResponse struct {
	Documento string
	Status    string
}
