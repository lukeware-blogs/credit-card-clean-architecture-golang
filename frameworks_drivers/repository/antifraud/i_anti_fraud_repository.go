package antifraud

type IAntiFraudRepository interface {
	ClienteFraudador(documento string) (*ClienteFraudadorResponse, error)
}
