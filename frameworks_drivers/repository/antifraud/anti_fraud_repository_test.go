package antifraud

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/enterprise_business/entities/cartao"
	datasource "gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/frameworks_drivers/datasource/mongo"
	"gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/frameworks_drivers/properties"
	"testing"
)

func Test_devera_buscar_dados_no_banco_com_sucesso(t *testing.T) {
	t.Parallel()

	assertions := assert.New(t)

	iProperties := properties.NewProperties("$GOPATH/.env")
	mongoDataSource := datasource.NewMongoDataSource(iProperties.Load())

	iAntiFraudRepository := NewAntiFraudRepository(mongoDataSource)

	clienteFraudador, err := iAntiFraudRepository.ClienteFraudador("888.888.888-88")

	assertions.NoError(err)
	assertions.NotNil(clienteFraudador)
	assertions.NotNil(clienteFraudador.Status)
	assertions.NotNil(clienteFraudador.Documento)
	assertions.Equal(cartao.CLIENTE_FRAUDADOR, clienteFraudador.Status)
}
