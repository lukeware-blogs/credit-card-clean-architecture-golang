package antifraud

import (
	"context"
	datasource "gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/frameworks_drivers/datasource/mongo"
	"go.mongodb.org/mongo-driver/bson"
)

const DATA_BASE_LABSIT, TABLE_ANTI_FRAUDE_CLIENTE = "labsit", "anti_fraude_cliente"

type antiFraudRepository struct {
	mongoDataSource datasource.IMongoDataSource
}

func NewAntiFraudRepository(mongoDataSource datasource.IMongoDataSource) IAntiFraudRepository {
	return antiFraudRepository{
		mongoDataSource: mongoDataSource,
	}
}

func (a antiFraudRepository) ClienteFraudador(documento string) (*ClienteFraudadorResponse, error) {
	response := ClienteFraudadorResponse{}
	connect, _ := a.mongoDataSource.Connect()
	defer a.mongoDataSource.Disconnect(connect)
	collection := a.mongoDataSource.DataSource(connect, DATA_BASE_LABSIT, TABLE_ANTI_FRAUDE_CLIENTE)

	err := collection.FindOne(context.TODO(), bson.D{{"documento", documento}}).Decode(&response)

	if err != nil {
		return nil, err

	}
	return &response, nil
}
