package score

import (
	datasource "gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/frameworks_drivers/datasource/postgres"
)

type scoreRepository struct {
	datasource datasource.IPostgresDataSource
}

func NewScoreRepository(datasource datasource.IPostgresDataSource) IScoreRepository {
	return scoreRepository{datasource: datasource}
}

func (s scoreRepository) ObterScore(documento string, tipoDocumento string) (*ScoreMapper, error) {
	defer s.datasource.Disconnect()
	s.datasource.Connect()
	client := s.datasource.Client()

	scoreMapper := ScoreMapper{}

	query := "select p.documento, p.tipo_pessoa, p.score from score_cliente p where p.documento=$1 and p.tipo_pessoa=$2"
	err := client.QueryRow(query, documento, tipoDocumento).Scan(&scoreMapper.Documento, &scoreMapper.TipoPessoa, &scoreMapper.Score)
	if err != nil {
		return &scoreMapper, err
	}
	return &scoreMapper, nil
}
