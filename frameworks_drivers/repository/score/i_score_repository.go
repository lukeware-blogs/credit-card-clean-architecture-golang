package score

type IScoreRepository interface {
	ObterScore(documento string, tipoDocumento string) (*ScoreMapper, error)
}
