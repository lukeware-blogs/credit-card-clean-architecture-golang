package score

type ScoreMapper struct {
	Documento  string
	TipoPessoa string
	Score      int
}
