package score

import (
	"github.com/stretchr/testify/assert"
	datasource "gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/frameworks_drivers/datasource/postgres"
	"gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/frameworks_drivers/properties"
	"testing"
)

func Test_buscar_score_no_banco(t *testing.T) {
	t.Parallel()

	assertions := assert.New(t)

	iProperties := properties.NewProperties("$GOPATH/.env")

	postgresDataSource := datasource.NewPostgresDataSource(iProperties.Load())

	iScoreRepository := NewScoreRepository(postgresDataSource)

	score, err := iScoreRepository.ObterScore("999.999.999-99", "PF")

	assertions.NoError(err)
	assertions.Equal(1000, score.Score)
	assertions.Equal("999.999.999-99", score.Documento)
	assertions.Equal("PF", score.TipoPessoa)
}

func Test_buscar_score_com_erro_na_conexao(t *testing.T) {
	t.Parallel()

	assertions := assert.New(t)

	iProperties := properties.NewProperties("$GOPATH/.env")

	load := iProperties.Load()
	load.DataBasePostgresUrl = ""
	postgresDataSource := datasource.NewPostgresDataSource(load)

	iScoreRepository := NewScoreRepository(postgresDataSource)

	_, err := iScoreRepository.ObterScore("999.999.999-99", "PF")

	assertions.EqualError(err, "pq: SSL is not enabled on the server")

}
