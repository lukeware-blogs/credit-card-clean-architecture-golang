package cartao

import (
	"context"
	usecaseCartao "gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/application_business/usecases/gerador_cartao"
	datasource "gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/frameworks_drivers/datasource/mongo"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

const DATA_BASE_LABSIT, TABLE_CATAO_CLIENTE = "labsit", "cartao_cliente"

type cartaoClienteRepository struct {
	mongoDataSource datasource.IMongoDataSource
}

func NewCartaoClienteRepository(mongoDataSource datasource.IMongoDataSource) ICartaoClienteRepository {
	return cartaoClienteRepository{
		mongoDataSource: mongoDataSource,
	}
}

func (c cartaoClienteRepository) Salvar(request usecaseCartao.CartaoClienteRequest) (string, error) {
	connect, _ := c.mongoDataSource.Connect()
	defer c.mongoDataSource.Disconnect(connect)
	collection := c.mongoDataSource.DataSource(connect, DATA_BASE_LABSIT, TABLE_CATAO_CLIENTE)

	response, err := collection.InsertOne(context.TODO(), request)
	if err != nil {
		return "", err
	}
	return response.InsertedID.(primitive.ObjectID).Hex(), nil
}

func (c cartaoClienteRepository) PorDocumento(documento string) ([]usecaseCartao.CartaoClienteResponse, error) {
	connect, _ := c.mongoDataSource.Connect()
	defer c.mongoDataSource.Disconnect(connect)
	collection := c.mongoDataSource.DataSource(connect, DATA_BASE_LABSIT, TABLE_CATAO_CLIENTE)

	find, err := collection.Find(context.TODO(), bson.D{{"documento", documento}})
	defer find.Close(context.Background())

	if err != nil {
		return nil, err
	}

	var result []usecaseCartao.CartaoClienteResponse
	err = find.All(context.Background(), &result)

	if err != nil {
		return nil, err
	}
	return result, nil
}

func (c cartaoClienteRepository) Todos() ([]usecaseCartao.CartaoClienteResponse, error) {
	connect, _ := c.mongoDataSource.Connect()
	defer c.mongoDataSource.Disconnect(connect)
	collection := c.mongoDataSource.DataSource(connect, DATA_BASE_LABSIT, TABLE_CATAO_CLIENTE)

	find, err := collection.Find(context.TODO(), bson.D{{}})
	defer find.Close(context.Background())

	if err != nil {
		return nil, err
	}

	var result []usecaseCartao.CartaoClienteResponse
	err = find.All(context.Background(), &result)

	if err != nil {
		return nil, err
	}
	return result, nil
}
