package cartao

import usecaseCartao "gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/application_business/usecases/gerador_cartao"

type ICartaoClienteRepository interface {
	Salvar(request usecaseCartao.CartaoClienteRequest) (string, error)
	Todos() ([]usecaseCartao.CartaoClienteResponse, error)
	PorDocumento(documento string) ([]usecaseCartao.CartaoClienteResponse, error)
}
