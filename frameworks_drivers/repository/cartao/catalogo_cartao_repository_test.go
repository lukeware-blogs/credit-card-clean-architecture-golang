package cartao

import (
	"github.com/stretchr/testify/assert"
	datasource2 "gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/frameworks_drivers/datasource/postgres"
	"gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/frameworks_drivers/properties"
	"testing"
)

func Test_devera_listar_os_catalogos_de_cartao(t *testing.T) {
	t.Parallel()
	assertions := assert.New(t)

	newProperties := properties.NewProperties("$GOPATH/.env")
	dataSource := datasource2.NewPostgresDataSource(newProperties.Load())
	repository := NewCatalogoCartaoRepository(dataSource)

	todos, err := repository.Todos()

	assertions.NoError(err)
	assertions.NotNil(todos)
	assertions.True(len(todos) > 0)
	assertions.Equal("GOLD", todos[0].Nome)
	assertions.Equal("MASTER_CARD", todos[0].Bandeira)
	assertions.Equal(4.99, todos[0].Anuidade)

}
