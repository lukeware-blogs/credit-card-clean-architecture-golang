package cartao

import (
	datasource "gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/frameworks_drivers/datasource/postgres"
	"log"
)

type catalogoCartaoRepository struct {
	datasource datasource.IPostgresDataSource
}

func NewCatalogoCartaoRepository(datasource datasource.IPostgresDataSource) ICatalogoCartaoRepository {
	return catalogoCartaoRepository{
		datasource: datasource,
	}
}

func (c catalogoCartaoRepository) Todos() ([]CatalogoCartaoMapper, error) {
	defer c.datasource.Disconnect()
	c.datasource.Connect()
	client := c.datasource.Client()

	catalogoCartao := []CatalogoCartaoMapper{}

	query := "select c.bandeira, c.nome, c.anuidade from catalogo_cartao c"
	rows, err := client.Query(query)
	if err != nil {
		log.Println(err)
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		mapper := CatalogoCartaoMapper{}

		if err := rows.Scan(&mapper.Bandeira, &mapper.Nome, &mapper.Anuidade); err != nil {
			log.Fatal(err)
			return []CatalogoCartaoMapper{}, err
		}

		catalogoCartao = append(catalogoCartao, mapper)

	}
	if err := rows.Err(); err != nil {
		log.Fatal(err)
		return []CatalogoCartaoMapper{}, err
	}

	return catalogoCartao, nil
}
