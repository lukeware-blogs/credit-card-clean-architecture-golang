package cartao

import (
	"github.com/stretchr/testify/assert"
	usecaseGeradorCartao "gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/application_business/usecases/gerador_cartao"
	"gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/enterprise_business/entities/cliente"
	datasource "gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/frameworks_drivers/datasource/mongo"
	"gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/frameworks_drivers/properties"
	"gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/frameworks_drivers/utility"
	"testing"
)

func Test_devera_salvar_cartao_cliente_com_sucesso(t *testing.T) {
	t.Parallel()

	assertions := assert.New(t)

	gerargorNumeroCartao := utility.NewGeradorNumeroCartao()
	newProperties := properties.NewProperties("$GOPATH/.env")
	load := newProperties.Load
	mongoDataSource := datasource.NewMongoDataSource(load())
	iCartaoRepository := NewCartaoClienteRepository(mongoDataSource)

	id, err := iCartaoRepository.Salvar(usecaseGeradorCartao.CartaoClienteRequest{
		Tipo:      cliente.PF,
		Documento: "999.999.999-99",
		Email:     "diego.dm.morais@gmail.com",
		Telefone:  "+55 19 9 9999-9999",
		Cartao: usecaseGeradorCartao.CartaoResponse{
			DiaVencimanto: 1,
			Titular:       "Diego Silva Morais",
			Limite:        15000.0,
			Serie:         gerargorNumeroCartao.GerarNumeroSerie(),
			Cvc:           gerargorNumeroCartao.GerarCvc(),
		},
	})

	assertions.NoError(err)
	assertions.NotNil(id)
}

func Test_devera_listar_os_cartoes_gerardos(t *testing.T) {
	t.Parallel()

	assertions := assert.New(t)

	newProperties := properties.NewProperties("$GOPATH/.env")
	load := newProperties.Load
	mongoDataSource := datasource.NewMongoDataSource(load())
	iCartaoRepository := NewCartaoClienteRepository(mongoDataSource)
	gerargorNumeroCartao := utility.NewGeradorNumeroCartao()

	_, err := iCartaoRepository.Salvar(usecaseGeradorCartao.CartaoClienteRequest{
		Tipo:      cliente.PF,
		Documento: "999.999.999-99",
		Email:     "diego.dm.morais@gmail.com",
		Telefone:  "+55 19 9 9999-9999",
		Cartao: usecaseGeradorCartao.CartaoResponse{
			DiaVencimanto: 1,
			Titular:       "Diego Silva Morais",
			Limite:        15000.0,
			Serie:         gerargorNumeroCartao.GerarNumeroSerie(),
			Cvc:           gerargorNumeroCartao.GerarCvc(),
		},
	})

	todos, err := iCartaoRepository.Todos()

	assertions.NoError(err)
	assertions.True(len(todos) > 0)
}

func Test_devera_listar_os_cartoes_gerardos_por_documentos(t *testing.T) {
	t.Parallel()

	assertions := assert.New(t)

	newProperties := properties.NewProperties("$GOPATH/.env")
	load := newProperties.Load
	mongoDataSource := datasource.NewMongoDataSource(load())
	iCartaoRepository := NewCartaoClienteRepository(mongoDataSource)
	gerargorNumeroCartao := utility.NewGeradorNumeroCartao()

	_, err := iCartaoRepository.Salvar(usecaseGeradorCartao.CartaoClienteRequest{
		Tipo:      cliente.PF,
		Documento: "999.999.999-99",
		Email:     "diego.dm.morais@gmail.com",
		Telefone:  "+55 19 9 9999-9999",
		Cartao: usecaseGeradorCartao.CartaoResponse{
			DiaVencimanto: 1,
			Titular:       "Diego Silva Morais",
			Limite:        15000.0,
			Serie:         gerargorNumeroCartao.GerarNumeroSerie(),
			Cvc:           gerargorNumeroCartao.GerarCvc(),
		},
	})

	todos, err := iCartaoRepository.PorDocumento("999.999.999-99")

	assertions.NoError(err)
	assertions.True(len(todos) > 0)
}
