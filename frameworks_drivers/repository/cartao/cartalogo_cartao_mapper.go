package cartao

const (
	MASTER_CARD = "MASTER_CARD"
	VISA_CARD   = "VISA_CARD"
)

type CatalogoCartaoMapper struct {
	Bandeira string
	Nome     string
	Anuidade float64
}
