package cartao

type ICatalogoCartaoRepository interface {
	Todos() ([]CatalogoCartaoMapper, error)
}
