package datasource

import (
	"gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/frameworks_drivers/properties"
	"log"

	"database/sql"

	_ "github.com/lib/pq"
)

type postgresDataSource struct {
	clientOptions *sql.DB
	properties    properties.PropertiesResponse
}

func NewPostgresDataSource(properties properties.PropertiesResponse) IPostgresDataSource {
	return &postgresDataSource{properties: properties}
}

func (c *postgresDataSource) Connect() error {
	var err error
	c.clientOptions, err = sql.Open("postgres", c.properties.DataBasePostgresUrl)
	if err != nil {
		log.Println(err)
		return err
	}
	pingErr := c.clientOptions.Ping()
	if pingErr != nil {
		log.Println(pingErr)
		return pingErr
	}
	log.Println("Connection to Postgres opened.")
	return nil
}

func (c *postgresDataSource) Disconnect() (bool, error) {
	err := c.clientOptions.Close()
	if err != nil {
		log.Println(err)
		return false, err
	}
	log.Println("Connection to Postgres closed.")
	return true, nil
}

func (c *postgresDataSource) Client() *sql.DB {
	return c.clientOptions
}
