package datasource

import "database/sql"

type IPostgresDataSource interface {
	Connect() error
	Disconnect() (bool, error)
	Client() *sql.DB
}
