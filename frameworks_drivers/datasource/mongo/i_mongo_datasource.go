package datasource

import "go.mongodb.org/mongo-driver/mongo"

type IMongoDataSource interface {
	Connect() (*mongo.Client, error)

	Disconnect(client *mongo.Client) (bool, error)

	DataSource(client *mongo.Client, database, collection string) *mongo.Collection
}
