package graphql

import (
	"github.com/99designs/gqlgen/graphql/handler"
	"github.com/99designs/gqlgen/graphql/playground"
	"github.com/labstack/echo/v4"
	"gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/graph"
	"gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/graph/generated"
	"net/http"
)

type GraphqlController struct {
	graphqlHandler    *handler.Server
	playgroundHandler http.HandlerFunc
}

func NewGraphqlController() GraphqlController {
	graphqlHandler := handler.NewDefaultServer(generated.NewExecutableSchema(generated.Config{Resolvers: &graph.Resolver{}}))
	playgroundHandler := playground.Handler("GraphQL", "/query")
	return GraphqlController{
		graphqlHandler:    graphqlHandler,
		playgroundHandler: playgroundHandler,
	}
}

func (g GraphqlController) Playground(echo echo.Context) error {
	g.playgroundHandler.ServeHTTP(echo.Response(), echo.Request())
	return nil
}

func (g GraphqlController) Query(echo echo.Context) error {
	g.graphqlHandler.ServeHTTP(echo.Response(), echo.Request())
	return nil
}
