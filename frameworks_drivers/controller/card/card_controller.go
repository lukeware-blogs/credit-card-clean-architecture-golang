package card

import (
	"github.com/labstack/echo/v4"
	usecaseCartao "gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/application_business/usecases/cartao"
	usecaseGeradorCartao "gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/application_business/usecases/gerador_cartao"
	usecaseScore "gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/application_business/usecases/score"
	datasourceMongo "gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/frameworks_drivers/datasource/mongo"
	datasourcePostgres "gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/frameworks_drivers/datasource/postgres"
	frameworkNotificator "gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/frameworks_drivers/notificator"
	frameworkProperties "gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/frameworks_drivers/properties"
	repositoryAntifraud "gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/frameworks_drivers/repository/antifraud"
	repositoryCartao "gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/frameworks_drivers/repository/cartao"
	repositortyScore "gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/frameworks_drivers/repository/score"
	controllerCartao "gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/interface_adapters/controller/cartao"
	gatewayAntiFraud "gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/interface_adapters/gateway/antifraud"
	gatewayCartao "gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/interface_adapters/gateway/cartao"
	gatewayNotificador "gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/interface_adapters/gateway/notificador"
	gatewayScore "gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/interface_adapters/gateway/score"
	"gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/interface_adapters/presenters"
	"net/http"
)

// FIXME Implementar teste E2E aqui
type cardController struct {
	cartaoController controllerCartao.ICartaoController
}

func NewCardController() ICardController {

	presenter := presenters.NewCartaoPresenter()
	iProperties := frameworkProperties.NewProperties("$GOPATH/.env")
	mongoDataSource := datasourceMongo.NewMongoDataSource(iProperties.Load())
	repository := repositoryAntifraud.NewAntiFraudRepository(mongoDataSource)
	gateway := gatewayAntiFraud.NewAntiFraudGateway(repository)

	postgresDataSource := datasourcePostgres.NewPostgresDataSource
	scoreRepository := repositortyScore.NewScoreRepository(postgresDataSource(iProperties.Load()))
	scoreGateway := gatewayScore.NewScoreGateway(scoreRepository)
	scoreInteractor := usecaseScore.NewScoreInteractor(scoreGateway)

	cartaoRepository := repositoryCartao.NewCartaoClienteRepository(mongoDataSource)
	cartaoGateway := gatewayCartao.NewGeradorCartaoGateway(cartaoRepository)

	notificadorGateway := gatewayNotificador.NewNotificadorGateway(frameworkNotificator.NewNotificadorMessageApi(iProperties.Load()))

	geradorCartao := usecaseGeradorCartao.NewGeradorCartao(cartaoGateway, notificadorGateway)

	interactor := usecaseCartao.NewCartaoInteractor(presenter, gateway, scoreInteractor, geradorCartao)
	cartaoController := controllerCartao.NewCartaoController(interactor)

	return cardController{
		cartaoController: cartaoController,
	}
}

func (c cardController) Create(echo echo.Context) error {
	cardDto := new(controllerCartao.CardDto)
	err := echo.Bind(cardDto)
	if err != nil {
		errorResponse := new(ErrorResponse)
		errorResponse.Error = err.Error()
		return echo.JSON(http.StatusInternalServerError, errorResponse)
	}
	response := c.cartaoController.Criar(*cardDto)
	if response.Error != "" {
		return echo.JSON(http.StatusInternalServerError, response)
	}

	return echo.JSON(http.StatusCreated, response)
}
