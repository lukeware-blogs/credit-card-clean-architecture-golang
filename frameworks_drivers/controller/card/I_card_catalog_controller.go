package card

import "github.com/labstack/echo/v4"

type ICardCatalogController interface {
	FindAll(echo echo.Context) error
}
