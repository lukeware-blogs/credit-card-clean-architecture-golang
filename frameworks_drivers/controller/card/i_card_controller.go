package card

import "github.com/labstack/echo/v4"

type ICardController interface {
	Create(echo echo.Context) error
}
