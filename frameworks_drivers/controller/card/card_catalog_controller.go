package card

import (
	"github.com/labstack/echo/v4"
	"gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/application_business/usecases/catalogo_cartoes"
	datasourcePostgres "gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/frameworks_drivers/datasource/postgres"
	frameworkProperties "gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/frameworks_drivers/properties"
	repositoryCartao "gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/frameworks_drivers/repository/cartao"
	controllerCartao "gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/interface_adapters/controller/cartao"
	"gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/interface_adapters/presenters"
	"net/http"
)

type cardCatalogController struct {
	catalogoCartaoController controllerCartao.ICatalogoCartaoController
}

func NewCatalogController() ICardCatalogController {
	iProperties := frameworkProperties.NewProperties("$GOPATH/.env")
	postgresDataSource := datasourcePostgres.NewPostgresDataSource(iProperties.Load())
	catalogoCartaoRepository := repositoryCartao.NewCatalogoCartaoRepository(postgresDataSource)
	iCatalogoCartaoPresenter := presenters.NewCatalogoCartaoPresenter()
	iCatalogoCartaoInteractor := catalogo_cartoes.NewCatalogoCartaoInteractor(catalogoCartaoRepository, iCatalogoCartaoPresenter)
	catalogoCartaoController := controllerCartao.NewCatalogoCartaoController(iCatalogoCartaoInteractor)

	return cardCatalogController{catalogoCartaoController: catalogoCartaoController}
}

func (c cardCatalogController) FindAll(echo echo.Context) error {
	todos, err := c.catalogoCartaoController.Todos()
	if err != nil {
		errorResponse := new(ErrorResponse)
		errorResponse.Error = err.Error()
		return echo.JSON(http.StatusInternalServerError, errorResponse)
	}
	return echo.JSON(http.StatusOK, todos)
}
