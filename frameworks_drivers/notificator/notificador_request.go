package notificator

type NotificadorRequest struct {
	Email        string
	Mensagem     string
	Destinatario string
	Titulo       string
}
