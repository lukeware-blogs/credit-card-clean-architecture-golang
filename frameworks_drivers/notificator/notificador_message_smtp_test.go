package notificator

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/frameworks_drivers/properties"
	"testing"
)

func Test_devera_enviar_push_para_email(t *testing.T) {
	t.Parallel()

	assertions := assert.New(t)

	newProperties := properties.NewProperties("$GOPATH/.env")

	notificador := NewNotificadorMessageSmpt(newProperties.Load())

	err := notificador.Push(NotificadorRequest{
		Email:        "diego.dm.morais@gmail.com",
		Mensagem:     "Estamos realizando teste automatizado ignore esse email",
		Destinatario: "Diego Silva Morais",
		Titulo:       "Teste automatizado golang api",
	})

	assertions.NoError(err)

}

func Test_devera_validar_o_envio_com_a_chave_errada(t *testing.T) {
	t.Parallel()

	assertions := assert.New(t)

	newProperties := properties.NewProperties("$GOPATH/.env")

	properties := newProperties.Load()
	properties.ApiKey = "chave_errada"

	notificador := NewNotificadorMessageSmpt(properties)

	err := notificador.Push(NotificadorRequest{
		Email:        "diego.dm.morais@gmail.com",
		Mensagem:     "Estamos realizando teste automatizado ignore esse email",
		Destinatario: "Diego Silva Morais",
		Titulo:       "Teste automatizado golang api",
	})

	assertions.Contains(err.Error(), "The provided authorization grant is invalid, expired, or revoked")

}

func Test_devera_validar_erro_no_envio_de_email(t *testing.T) {
	t.Parallel()

	assertions := assert.New(t)

	newProperties := properties.NewProperties("$GOPATH/.env")

	properties := newProperties.Load()
	properties.ApiKey = ""
	properties.EmailAdmin = ""

	notificador := NewNotificadorMessageSmpt(properties)

	err := notificador.Push(NotificadorRequest{
		Email:        "",
		Mensagem:     "",
		Destinatario: "",
		Titulo:       "",
	})

	assertions.Contains(err.Error(), "451 Authentication failed: Could not authenticate")

}
