package notificator

import (
	"gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/frameworks_drivers/properties"
	"net/smtp"
)

const (
	USERNAME = "apikey"
)

type notificadorMessageSmtp struct {
	properties properties.PropertiesResponse
}

func NewNotificadorMessageSmpt(properties properties.PropertiesResponse) INotificadorMessage {
	return notificadorMessageSmtp{properties: properties}
}

func (n notificadorMessageSmtp) Push(request NotificadorRequest) error {

	from := n.properties.EmailAdmin

	user := USERNAME
	password := n.properties.ApiKey

	to := []string{request.Email}

	addr := n.properties.SmtpHost + ":" + n.properties.SmtpPort
	host := n.properties.SmtpHost

	msg := []byte("From: " + n.properties.EmailAdmin + "\r\n" +
		"To: " + request.Email + "\r\n" +
		"Subject: " + request.Titulo + "\r\n\r\n" +
		"" + request.Mensagem + "\r\n")

	auth := smtp.PlainAuth("", user, password, host)

	err := smtp.SendMail(addr, auth, from, to, msg)
	if err != nil {
		return err
	}
	return nil
}
