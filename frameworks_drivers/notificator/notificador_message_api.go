package notificator

import (
	"errors"
	"fmt"
	"github.com/sendgrid/sendgrid-go"
	"github.com/sendgrid/sendgrid-go/helpers/mail"
	"gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/frameworks_drivers/properties"
)

type notificadorMessageApi struct {
	properties properties.PropertiesResponse
}

func NewNotificadorMessageApi(properties properties.PropertiesResponse) INotificadorMessage {
	return notificadorMessageApi{properties: properties}
}

func (n notificadorMessageApi) Push(request NotificadorRequest) error {

	from := mail.NewEmail(n.properties.NameAdmin, n.properties.EmailAdmin)
	subject := request.Titulo
	to := mail.NewEmail(request.Destinatario, request.Email)
	plainTextContent := request.Mensagem
	htmlContent := "<strong>" + request.Mensagem + "</strong>"
	message := mail.NewSingleEmail(from, subject, to, plainTextContent, htmlContent)
	client := sendgrid.NewSendClient(n.properties.ApiKey)
	response, _ := client.Send(message)

	fmt.Println(response.StatusCode)
	fmt.Println(response.Body)
	fmt.Println(response.Headers)
	if response.StatusCode != 202 {
		return errors.New(response.Body)
	}
	return nil
}
