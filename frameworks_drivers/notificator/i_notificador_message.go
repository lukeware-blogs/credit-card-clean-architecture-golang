package notificator

type INotificadorMessage interface {
	Push(request NotificadorRequest) error
}
