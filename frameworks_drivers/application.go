package frameworks_drivers

import (
	"github.com/labstack/echo/v4"
	"gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/frameworks_drivers/controller/card"
	"gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/frameworks_drivers/controller/graphql"
	"gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/frameworks_drivers/properties"
)

type Application struct {
}

func (a Application) Init() {
	propertiesResponse := properties.NewProperties("$GOPATH/.env").Load()

	cardController := card.NewCardController()
	catalogController := card.NewCatalogController()
	graphqlController := graphql.NewGraphqlController()

	e := echo.New()

	e.POST("/cards/customers", cardController.Create)
	e.GET("/cards/catalogs", catalogController.FindAll)
	e.POST("/query", graphqlController.Query)
	e.GET("/playground", graphqlController.Playground)

	e.Logger.Fatal(e.Start(":" + propertiesResponse.PortServer))
}
