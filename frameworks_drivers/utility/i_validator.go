package utility

type IValidator interface {
	ValidateEmail(email string) error
	ValidateNumberCard(number string) bool
	IsEmpty(value string) bool
}
