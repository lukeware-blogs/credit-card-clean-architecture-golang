package utility

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func Test_devera_validar_email(t *testing.T) {
	t.Parallel()
	assertions := assert.New(t)

	newValidator := NewValidator()

	err := newValidator.ValidateEmail("diego.morais@gmail.com")

	assertions.NoError(err)
}

func Test_devera_validar_email_invalido(t *testing.T) {
	t.Parallel()
	assertions := assert.New(t)

	v := NewValidator()

	err := v.ValidateEmail("email_invalido")

	assertions.EqualError(err, "email inválido")
}

func Test_devera_validar_string_vazia(t *testing.T) {
	t.Parallel()
	assertions := assert.New(t)

	validator := NewValidator()

	isEmpty := validator.IsEmpty("")

	assertions.Equal(true, isEmpty)
}

func Test_devera_validar_o_numero_cartao(t *testing.T) {
	t.Parallel()
	assertions := assert.New(t)

	validator := NewValidator()

	isValid := validator.ValidateNumberCard("9999 9999 9999 9999")
	isValid1 := validator.ValidateNumberCard("9999 9999")
	isValid2 := validator.ValidateNumberCard("9999 9999 9999 ABCD")
	isValid3 := validator.ValidateNumberCard("")

	assertions.Equal(true, isValid)
	assertions.Equal(false, isValid1)
	assertions.Equal(false, isValid2)
	assertions.Equal(false, isValid3)
}
