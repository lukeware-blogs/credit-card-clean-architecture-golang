package utility

import (
	"errors"
	"regexp"
	"strings"
)

type validator struct {
}

func NewValidator() IValidator {
	return validator{}
}

func (v validator) ValidateEmail(email string) error {
	re := regexp.MustCompile("^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$")
	if re.MatchString(email) {
		return nil
	}
	return errors.New("email inválido")
}

func (v validator) ValidateNumberCard(number string) bool {
	noEmpty := !v.IsEmpty(number)
	amoutDigits := len(strings.ReplaceAll(number, " ", "")) == 16
	isValid := !v.containCharacters(number)
	return noEmpty && amoutDigits && isValid
}

func (v validator) IsEmpty(value string) bool {
	return len(strings.TrimSpace(strings.ReplaceAll(value, " ", ""))) == 0
}

func (v validator) containCharacters(number string) bool {
	matched, _ := regexp.MatchString(`[A-Za-z]`, strings.ReplaceAll(number, " ", ""))
	return matched
}
