package utility

type IGeradorNumeroCartao interface {
	GerarNumeroSerie() string
	GerarCvc() string
}
