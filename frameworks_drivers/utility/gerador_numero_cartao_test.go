package utility

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func Test_devera_gerar_numero_do_cartao_com_sucesso(t *testing.T) {

	t.Parallel()

	assertions := assert.New(t)

	newGeradorNumeroCartao := NewGeradorNumeroCartao()

	serie := newGeradorNumeroCartao.GerarNumeroSerie()

	assertions.NotNil(serie)

}

func Test_devera_gerar_cvv_com_sucesso(t *testing.T) {

	t.Parallel()

	assertions := assert.New(t)

	newGeradorNumeroCartao := NewGeradorNumeroCartao()

	cvv := newGeradorNumeroCartao.GerarCvc()

	assertions.NotNil(cvv)

}
