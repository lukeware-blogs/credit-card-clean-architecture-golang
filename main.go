package main

import "gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/frameworks_drivers"

func main() {
	new(frameworks_drivers.Application).Init()
}
