# Go Mod

```shell
go mod tidy
```

# Comandos coverage

  ```shell
  go test ./...
  ```

  ```shell
  go test -coverprofile=coverage.out ./... && go tool cover -func=coverage.out
  ```

  ```shell
  go test ./... --coverprofile=cover.out && go tool cover --html=cover.out
  ```

# Comando para compilar

  ```shell
  go build
  ```

# Arquivo properties

- crie um arquivo .env-test no diretorio $GOPATH/ com as propriedades abaixo.
  ```
  EMAIL_API_KEY=chave_de_acesso
  EMAIL_ADMIN=empresa@gmail.com
  EMAIL_SMTP_HOST=smtp.sendgrid.net
  EMAIL_SMTP_PORT=587
  EMAIL_ADMIN_NAME=Empresa
  DATABASE_POSTGRES_URL="host=127.0.0.1 port=5432 user=postgres password=admin123 dbname=labsit sslmode=disable"
  DATABASE_MONGODB_URL=mongodb://admin:admin123@127.0.0.1:27017
  ```


- crie um arquivo do diretorio $GOPATH/.env
- copie cole as propriedades abaixo
- Configuracao email smtp e email api
    - Utilizando o <b>Sendgrid</b> para envio de email

  ```
  EMAIL_SMTP_HOST=smtp.sendgrid.net
  EMAIL_SMTP_PORT=587
  
  EMAIL_API_KEY=<api key criada no sendgrid>
  EMAIL_ADMIN=<email administrador configurado no sendgrid>
  EMAIL_ADMIN_NAME=<nome apresentado ao destinatários>
  ```

- Configuração do banco de dados
  ```
  DATABASE_POSTGRES_URL="host=127.0.0.1 port=5432 user=postgres password=admin123 dbname=labsit sslmode=disable"
  DATABASE_MONGODB_URL=mongodb://admin:admin123@127.0.0.1:27017
  ```
- Configuração da porta do serviço
  ```
  PORT_SERVER=1323
  ```

# Comandos Docker

  ```shell
  docker-compose up -d postgres pgadmin4 mongo mongo-express
  ```
  ou
  ```shell
    docker-compose up -d
  ```
# Login PgAdmin4 Postgres

- Hostname/address: postgres
- Port: 5432
- Maintenance database: postgres
- Username: postgres
- Password: admin123
- ![img.png](img.png)

# Script postgres

  ```sql
    -- Database: labsit
    -- DROP DATABASE IF EXISTS labsit;

    CREATE DATABASE labsit
      WITH
      OWNER = postgres
      ENCODING = 'UTF8'
      LC_COLLATE = 'en_US.utf8'
      LC_CTYPE = 'en_US.utf8'
      TABLESPACE = pg_default
      CONNECTION LIMIT = -1;
    
    
    -- Table: public.catalogo_cartao
    -- DROP TABLE IF EXISTS public.catalogo_cartao;
    
    CREATE TABLE IF NOT EXISTS public.catalogo_cartao
    (
      nome character varying(250) COLLATE pg_catalog."default" NOT NULL,
      bandeira character varying(250) COLLATE pg_catalog."default" NOT NULL,
      anuidade double precision NOT NULL,
      CONSTRAINT catalogo_cartao_pkey PRIMARY KEY (nome)
      )
    
      TABLESPACE pg_default;
    
    ALTER TABLE IF EXISTS public.catalogo_cartao
      OWNER to postgres;
    
    COMMENT ON TABLE public.catalogo_cartao
        IS 'tabela utilizada para salvar cartões do catalogo';

    INSERT INTO public.catalogo_cartao(
      nome, bandeira, anuidade)
    VALUES ('PLATINUM GOLDEN', 'MASTER_CARD', 4.99);

    
    -- Table: public.score_cliente
    -- DROP TABLE IF EXISTS public.score_cliente;
    
    CREATE TABLE IF NOT EXISTS public.score_cliente
    (
      documento character varying(250) COLLATE pg_catalog."default" NOT NULL,
      tipo_pessoa character varying(250) COLLATE pg_catalog."default" NOT NULL,
      score integer NOT NULL,
      CONSTRAINT score_cliente_pkey PRIMARY KEY (documento)
      )
    
      TABLESPACE pg_default;
    
    ALTER TABLE IF EXISTS public.score_cliente
      OWNER to postgres;

    COMMENT ON TABLE public.catalogo_cartao
        IS 'tabela utilizada para salvar o score do cliente';
    
    INSERT INTO public.score_cliente(
      documento, tipo_pessoa, score)
    VALUES ('999.888.777-66', 'PF', 800);
  ```

# Script mongodb
  - Adicionar um fraudador no mongo 
  - ![img_1.png](img_1.png)
  ```
  {
          "_id": ObjectID(),
          documento: '999.888.777-66',
          status: 'CLIENTE_FRAUDADOR'
  }
  ```

# Links

- Link para acessar mongo-express: http://localhost:8081/
- Link para acessar pgadmin4: http://localhost:8082/browser/
    - Usuario: admin@admin.com
    - Senha: admin123
- Link graphql http://localhost:8083/playground

# Graphql na criação de projeto

- comando para configurar o graphql
  ```shell
  go get github.com/99designs/gqlgen
  ```
- criar o arquivo [gplgen.yml](gqlgen.yml)
    - obs.: esse arquivo case não será alterado
- criar o arquivo [tools.go](tools.go)
- criar a pasta ./graph
- criar arquivo [./graph/schema.graphqls](./graph/schema.graphqls)
- comando para atualizar o schema graphql
  ```shell
  go mod tidy
  go run github.com/99designs/gqlgen generate
  ```

# Consulta graphql
- Scripts cartoes de 1 único cliente
  ```graphql
  {
    CustomerCreditCardsDocument(document: "999.888.777-66") {
      document
      type
      email
      credCard {
        limit
        series
        annuity
      }
    }
  }
  ```
- Scripts listar todos os cartoes de todos os clientes
  ```graphql
  {
    CustomerCreditCards {
      document
      type
      email
      credCard {
        limit
        series
        annuity
      }
    }
  }
  ```

# Collection Postman
- segue a collection do postman para você testar a aplicação via postman
  [cartao_credito.postman_collection.json](./cartao_credito.postman_collection.json)

# Diagrama de negócio
![cartao-de-credito.drawio.png](./cartao-de-credito.drawio.png)

![projeto-cartao-credito-clean-architecture.png](./projeto-cartao-credito-clean-architecture.png)