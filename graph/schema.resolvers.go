package graph

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"

	useCaseCartaoCliente "gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/application_business/usecases/cartao_cliente"
	datasource "gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/frameworks_drivers/datasource/mongo"
	"gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/frameworks_drivers/properties"
	"gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/frameworks_drivers/repository/cartao"
	"gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/graph/generated"
	"gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/graph/model"
	controllerCartaoCliente "gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/interface_adapters/controller/cartao_cliente"
	"gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/interface_adapters/presenters"
)

// CustomerCreditCardsDocument is the resolver for the CustomerCreditCardsDocument field.
func (r *queryResolver) CustomerCreditCardsDocument(ctx context.Context, document string) ([]*model.CustomerCreditCard, error) {
	return cartaoClienteController.PorDocumento(document)
}

// CustomerCreditCards is the resolver for the CustomerCreditCards field.
func (r *queryResolver) CustomerCreditCards(ctx context.Context) ([]*model.CustomerCreditCard, error) {
	return cartaoClienteController.Todos()
}

// Query returns generated.QueryResolver implementation.
func (r *Resolver) Query() generated.QueryResolver { return &queryResolver{r} }

type queryResolver struct{ *Resolver }

// !!! WARNING !!!
// The code below was going to be deleted when updating resolvers. It has been copied here so you have
// one last chance to move it out of harms way if you want. There are two reasons this happens:
//   - When renaming or deleting a resolver the old code will be put in here. You can safely delete
//     it when you're done.
//   - You have helper methods in this file. Move them out to keep these resolver files clean.
var cartaoClienteController controllerCartaoCliente.ICartaoClienteController = NewCartaoClienteResolver()

func NewCartaoClienteResolver() controllerCartaoCliente.ICartaoClienteController {
	cartaoClientePresenter := presenters.NewCartaoClientePresenter()
	newProperties := properties.NewProperties("$GOPATH/.env")
	iMongoDataSource := datasource.NewMongoDataSource(newProperties.Load())
	cartaoClienteRepository := cartao.NewCartaoClienteRepository(iMongoDataSource)
	iCartaoClienteInteractor := useCaseCartaoCliente.NewCartaoClienteInteractor(cartaoClientePresenter, cartaoClienteRepository)
	iCartaoClienteController := controllerCartaoCliente.NewCartaoClienteController(iCartaoClienteInteractor)
	return iCartaoClienteController
}
