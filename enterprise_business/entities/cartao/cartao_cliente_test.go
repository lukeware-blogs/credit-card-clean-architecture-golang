package cartao

import (
	"github.com/stretchr/testify/assert"
	entityCliente "gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/enterprise_business/entities/cliente"
	cartao2 "gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/frameworks_drivers/repository/cartao"
	"gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/frameworks_drivers/utility"
	"testing"
)

func Test_devera_validar_cartao_do_cliente(t *testing.T) {
	t.Parallel()

	assertions := assert.New(t)

	validator := utility.NewValidator()

	cartao := NewCartao("Diego Silva Morais", 4, "Gold", 4.99, cartao2.MASTER_CARD, validator)
	cliente := entityCliente.NewCliente(entityCliente.PF, "999.999.999-99", "diegomorais@gmail.com", "+55 (19) 9 9999-9999", validator)

	cartaoCliente := NewCartaoCliente(cartao, cliente, validator)

	err := cartaoCliente.EValido()

	assertions.NoError(err)
	assertions.NotNil(cartaoCliente.Cliente())
	assertions.Equal(entityCliente.PF, cartaoCliente.Cliente().Tipo())
	assertions.Equal("999.999.999-99", cartaoCliente.Cliente().Documento())
	assertions.Equal("diegomorais@gmail.com", cartaoCliente.Cliente().Email())
	assertions.Equal("+55 (19) 9 9999-9999", cartaoCliente.Cliente().Telefone())
	assertions.Equal("Diego Silva Morais", cartaoCliente.Cartao().Titular())
	assertions.Equal("Gold", cartaoCliente.Cartao().NomeCartao())
	assertions.Equal(cartao2.MASTER_CARD, cartaoCliente.Cartao().Bandeira())
	assertions.Equal(4.99, cartaoCliente.Cartao().Anuidade())
	assertions.Equal(4, cartaoCliente.Cartao().DiaVencimento())

}

func Test_devera_validar_cartao_do_cliente_com_cartao_invalido(t *testing.T) {
	t.Parallel()

	assertions := assert.New(t)

	validator := utility.NewValidator()
	cartao := NewCartao("Diego Silva Morais", 100, "Gold", 4.99, cartao2.MASTER_CARD, validator)
	cliente := entityCliente.NewCliente(entityCliente.PF, "999.999.999-99", "diegomorais@gmail.com", "+55 (19) 9 9999-9999", validator)

	cartaoCliente := NewCartaoCliente(cartao, cliente, validator)
	cartaoCliente2 := NewCartaoCliente(nil, cliente, validator)

	err := cartaoCliente.EValido()
	err2 := cartaoCliente2.EValido()

	assertions.EqualError(err, "dia de vencimento inválido")
	assertions.EqualError(err2, "cartão inválido")

}

func Test_devera_validar_cartao_do_cliente_com_cliente_invalido(t *testing.T) {
	t.Parallel()

	assertions := assert.New(t)

	validator := utility.NewValidator()
	cartao := NewCartao("Diego Silva Morais", 1, "Gold", 4.99, cartao2.MASTER_CARD, validator)
	cliente := entityCliente.NewCliente(entityCliente.PF, "", "diegomorais@gmail.com", "+55 (19) 9 9999-9999", validator)

	cartaoCliente := NewCartaoCliente(cartao, cliente, validator)
	cartaoCliente2 := NewCartaoCliente(cartao, nil, validator)

	err := cartaoCliente.EValido()
	err2 := cartaoCliente2.EValido()

	assertions.EqualError(err, "documento não informado")
	assertions.EqualError(err2, "clientel não informada")

}

func Test_devera_validar_cartao_do_cliente_com_nome_invalido(t *testing.T) {
	t.Parallel()

	assertions := assert.New(t)

	validator := utility.NewValidator()

	cartao := NewCartao("Diego Silva Morais", 4, "", 4.99, cartao2.MASTER_CARD, validator)
	cliente := entityCliente.NewCliente(entityCliente.PF, "999.999.999-99", "diegomorais@gmail.com", "+55 (19) 9 9999-9999", validator)

	cartaoCliente := NewCartaoCliente(cartao, cliente, validator)

	err := cartaoCliente.EValido()

	assertions.EqualError(err, "nome do cartão, não informado")

}

func Test_devera_validar_cartao_do_cliente_com_bandeira_invalido(t *testing.T) {
	t.Parallel()

	assertions := assert.New(t)

	validator := utility.NewValidator()

	cartao := NewCartao("Diego Silva Morais", 4, "Gold", 4.99, "", validator)
	cliente := entityCliente.NewCliente(entityCliente.PF, "999.999.999-99", "diegomorais@gmail.com", "+55 (19) 9 9999-9999", validator)

	cartaoCliente := NewCartaoCliente(cartao, cliente, validator)

	err := cartaoCliente.EValido()

	assertions.EqualError(err, "bandeira do cartão, não informado")

}
