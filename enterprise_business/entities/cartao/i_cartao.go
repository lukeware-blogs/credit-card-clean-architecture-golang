package cartao

type ICartao interface {
	EValido() error
	Titular() string
	DiaVencimento() int
	NomeCartao() string
	Bandeira() string
	Anuidade() float64
}
