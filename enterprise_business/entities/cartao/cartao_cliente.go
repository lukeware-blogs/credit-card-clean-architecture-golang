package cartao

import (
	"errors"
	entityCliente "gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/enterprise_business/entities/cliente"
	"gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/frameworks_drivers/utility"
)

const (
	CLIENTE_FRAUDADOR      = "CLIENTE_FRAUDADOR"
	CLIENTE_NAO_FRAUDADOR  = "CLIENTE_NAO_FRAUDADOR"
	CLIENTE_NAO_REGISTRADO = "CLIENTE_NAO_REGISTRADO"
)

type cartaoCliente struct {
	cartao    ICartao
	cliente   entityCliente.ICliente
	validator utility.IValidator
}

func NewCartaoCliente(cartao ICartao,
	cliente entityCliente.ICliente,
	validator utility.IValidator) ICartaoCliente {
	return cartaoCliente{
		cartao:    cartao,
		cliente:   cliente,
		validator: validator,
	}
}

func (c cartaoCliente) EValido() error {
	if c.cartao == nil {
		return errors.New("cartão inválido")
	}
	err := c.cartao.EValido()
	if err != nil {
		return err
	}
	if c.cliente == nil {
		return errors.New("clientel não informada")
	}
	errCliente := c.cliente.EValido()
	if errCliente != nil {
		return errCliente
	}
	return nil
}

func (c cartaoCliente) Cliente() entityCliente.ICliente { return c.cliente }
func (c cartaoCliente) Cartao() ICartao                 { return c.cartao }
