package cartao

import (
	"github.com/stretchr/testify/assert"
	cartao2 "gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/frameworks_drivers/repository/cartao"
	"gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/frameworks_drivers/utility"
	"testing"
)

func Test_devera_ser_um_cartao_valido(t *testing.T) {
	t.Parallel()

	assertions := assert.New(t)

	cartao := NewCartao("Diego Silva Morais", 1, "Gold", 4.99, cartao2.MASTER_CARD, utility.NewValidator())

	err := cartao.EValido()

	assertions.NoError(err)
	assertions.Equal("Diego Silva Morais", cartao.Titular())
	assertions.Equal(1, cartao.DiaVencimento())
}

func Test_devera_validar_o_titular_do_cartao(t *testing.T) {
	t.Parallel()

	assertions := assert.New(t)

	cartao := NewCartao("", 1, "Gold", 4.99, cartao2.MASTER_CARD, utility.NewValidator())

	err := cartao.EValido()

	assertions.EqualError(err, "titular não informado")
}

func Test_devera_validar_o_dia_de_vencimento(t *testing.T) {
	t.Parallel()

	assertions := assert.New(t)

	cartao := NewCartao("Diego Morais", 0, "Gold", 4.99, cartao2.MASTER_CARD, utility.NewValidator())
	cartao2 := NewCartao("Diego Morais", 40, "Gold", 4.99, cartao2.MASTER_CARD, utility.NewValidator())

	err := cartao.EValido()
	err2 := cartao2.EValido()

	assertions.EqualError(err, "dia de vencimento inválido")
	assertions.EqualError(err2, "dia de vencimento inválido")
}
