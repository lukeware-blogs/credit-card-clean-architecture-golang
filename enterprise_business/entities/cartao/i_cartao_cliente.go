package cartao

import entityCliente "gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/enterprise_business/entities/cliente"

type ICartaoCliente interface {
	EValido() error
	Cliente() entityCliente.ICliente
	Cartao() ICartao
}
