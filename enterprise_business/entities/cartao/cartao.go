package cartao

import (
	"errors"
	"gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/frameworks_drivers/utility"
)

type cartao struct {
	titular       string
	diaVencimento int
	nomeCartao    string
	anuidade      float64
	bandeira      string
	validador     utility.IValidator
}

func NewCartao(
	titular string,
	diaVencimento int,
	nomeCartao string,
	anuidade float64,
	bandeira string,
	validator utility.IValidator,
) ICartao {
	return cartao{
		titular:       titular,
		diaVencimento: diaVencimento,
		nomeCartao:    nomeCartao,
		anuidade:      anuidade,
		bandeira:      bandeira,
		validador:     validator,
	}
}

func (c cartao) EValido() error {
	if c.validador.IsEmpty(c.titular) {
		return errors.New("titular não informado")
	}
	if c.diaVencimento <= 0 || c.diaVencimento > 31 {
		return errors.New("dia de vencimento inválido")
	}
	if c.validador.IsEmpty(c.nomeCartao) {
		return errors.New("nome do cartão, não informado")
	}
	if c.validador.IsEmpty(c.bandeira) {
		return errors.New("bandeira do cartão, não informado")
	}
	return nil
}

func (c cartao) Titular() string    { return c.titular }
func (c cartao) DiaVencimento() int { return c.diaVencimento }

func (c cartao) NomeCartao() string { return c.nomeCartao }
func (c cartao) Bandeira() string   { return c.bandeira }
func (c cartao) Anuidade() float64  { return c.anuidade }
