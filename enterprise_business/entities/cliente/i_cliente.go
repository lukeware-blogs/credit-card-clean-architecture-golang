package cliente

type ICliente interface {
	EValido() error
	Tipo() string
	Documento() string
	Email() string
	Telefone() string
}
