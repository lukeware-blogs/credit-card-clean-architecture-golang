package cliente

import (
	"errors"
	"gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/frameworks_drivers/utility"
)

const (
	PJ = "PJ"
	PF = "PF"
)

func NewCliente(tipo string,
	documento string,
	email string,
	telefone string,
	validator utility.IValidator) ICliente {
	return cliente{
		tipo:      tipo,
		documento: documento,
		email:     email,
		telefone:  telefone,
		validator: validator,
	}
}

type cliente struct {
	tipo      string
	documento string
	email     string
	telefone  string
	validator utility.IValidator
}

func (c cliente) EValido() error {
	if c.validator.IsEmpty(c.tipo) {
		return errors.New("tipo não informado")
	}
	if c.tipo != PJ && c.tipo != PF {
		return errors.New("tipo inválido")
	}
	if c.validator.IsEmpty(c.documento) {
		return errors.New("documento não informado")
	}
	if c.validator.IsEmpty(c.email) {
		return errors.New("email não informado")
	}
	err := c.validator.ValidateEmail(c.email)
	if err != nil {
		return err
	}
	if c.validator.IsEmpty(c.telefone) {
		return errors.New("telefone não informado")
	}
	return nil
}

func (c cliente) Tipo() string      { return c.tipo }
func (c cliente) Documento() string { return c.documento }
func (c cliente) Email() string     { return c.email }
func (c cliente) Telefone() string  { return c.telefone }
