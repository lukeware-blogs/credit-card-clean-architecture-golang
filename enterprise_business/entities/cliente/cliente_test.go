package cliente

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/lukeware-blogs/credit-card-clean-architecture-golang/application/frameworks_drivers/utility"
	"testing"
)

func Test_devera_conter_dados_validos(t *testing.T) {
	t.Parallel()

	assertions := assert.New(t)

	clientel := NewCliente(PF, "999.999.999-99", "diegomorais@gmail.com", "+55 (19) 9 99999-9999", utility.NewValidator())
	err := clientel.EValido()

	assertions.NoError(err)
	assertions.Equal(PF, clientel.Tipo())
	assertions.Equal("999.999.999-99", clientel.Documento())
	assertions.Equal("diegomorais@gmail.com", clientel.Email())
	assertions.Equal("+55 (19) 9 99999-9999", clientel.Telefone())
}

func Test_devera_validar_email_invalido(t *testing.T) {
	t.Parallel()

	assertions := assert.New(t)

	clientel := NewCliente(PF, "999.999.999-999", "email_invalido", "+55 (19) 9 99999-9999", utility.NewValidator())
	err := clientel.EValido()

	assertions.EqualError(err, "email inválido")
}

func Test_devera_validar_email_em_branco(t *testing.T) {
	t.Parallel()

	assertions := assert.New(t)

	clientel := NewCliente(PJ, "999.999.999-999", "", "+55 (19) 9 99999-9999", utility.NewValidator())
	err := clientel.EValido()

	assertions.EqualError(err, "email não informado")
}

func Test_devera_validar_tipo_cliente_em_branco(t *testing.T) {
	t.Parallel()

	assertions := assert.New(t)

	clientel := NewCliente("", "999.999.999-999", "diegomorais@gmail.com", "+55 (19) 9 99999-9999", utility.NewValidator())
	err := clientel.EValido()

	assertions.EqualError(err, "tipo não informado")
}

func Test_devera_validar_documento_em_branco(t *testing.T) {
	t.Parallel()

	assertions := assert.New(t)

	clientel := NewCliente(PF, "", "diegomorais@gmail.com", "+55 (19) 9 99999-9999", utility.NewValidator())
	err := clientel.EValido()

	assertions.EqualError(err, "documento não informado")
}

func Test_devera_validar_telefone_em_branco(t *testing.T) {
	t.Parallel()

	assertions := assert.New(t)

	clientel := NewCliente(PF, "999.999.999-999", "diegomorais@gmail.com", "", utility.NewValidator())
	err := clientel.EValido()

	assertions.EqualError(err, "telefone não informado")
}

func Test_devera_conter_dados_com_tipo_invalido(t *testing.T) {
	t.Parallel()

	assertions := assert.New(t)

	clientel := NewCliente("TIPO_INVALIDO", "999.999.999-999", "diegomorais@gmail.com", "+55 (19) 9 99999-9999", utility.NewValidator())
	err := clientel.EValido()

	assertions.EqualError(err, "tipo inválido")
}
