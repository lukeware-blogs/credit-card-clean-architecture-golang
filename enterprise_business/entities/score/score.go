package score

import (
	"errors"
)

type score struct {
	pontos int
}

func NewScore(pontos int) IScore {
	return score{pontos: pontos}
}

func (s score) EValido() error {
	if s.pontos < 0 {
		return errors.New("pontos inválidos")
	}
	return nil
}

func (s score) Nivel() int {
	switch {
	case s.pontos > 0 && s.pontos <= 200:
		return 5
	case s.pontos > 200 && s.pontos <= 400:
		return 4
	case s.pontos > 400 && s.pontos <= 600:
		return 3
	case s.pontos > 600 && s.pontos <= 800:
		return 2
	default:
		return 1
	}
}
