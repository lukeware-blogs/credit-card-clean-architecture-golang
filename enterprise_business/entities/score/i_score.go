package score

type IScore interface {
	Nivel() int
	EValido() error
}
