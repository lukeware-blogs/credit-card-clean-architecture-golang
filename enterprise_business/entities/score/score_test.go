package score

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func Test_devera_validar_score(t *testing.T) {
	t.Parallel()

	assertions := assert.New(t)

	score := NewScore(100)
	err := score.EValido()

	assertions.NoError(err)
}

func Test_devera_validar_score_invalido(t *testing.T) {
	t.Parallel()

	assertions := assert.New(t)

	score := NewScore(-100)
	err := score.EValido()

	assertions.EqualError(err, "pontos inválidos")
}

func Test_devera_validar_score_nivel_5(t *testing.T) {
	t.Parallel()

	assertions := assert.New(t)

	score := NewScore(200)
	err := score.EValido()
	nivel := score.Nivel()

	assertions.NoError(err)

	assertions.Equal(5, nivel)
}

func Test_devera_validar_score_nivel_4(t *testing.T) {
	t.Parallel()

	assertions := assert.New(t)

	score := NewScore(400)
	err := score.EValido()
	nivel := score.Nivel()

	assertions.NoError(err)

	assertions.Equal(4, nivel)
}
func Test_devera_validar_score_nivel_3(t *testing.T) {
	t.Parallel()

	assertions := assert.New(t)

	score := NewScore(600)
	err := score.EValido()
	nivel := score.Nivel()

	assertions.NoError(err)

	assertions.Equal(3, nivel)
}

func Test_devera_validar_score_nivel_2(t *testing.T) {
	t.Parallel()

	assertions := assert.New(t)

	score := NewScore(800)
	err := score.EValido()
	nivel := score.Nivel()

	assertions.NoError(err)

	assertions.Equal(2, nivel)
}

func Test_devera_validar_score_nivel_1(t *testing.T) {
	t.Parallel()

	assertions := assert.New(t)

	score := NewScore(1000)
	err := score.EValido()
	nivel := score.Nivel()

	assertions.NoError(err)

	assertions.Equal(1, nivel)
}
